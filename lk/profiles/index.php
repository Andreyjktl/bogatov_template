<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список ответственных лиц");

if(!CModule::IncludeModule("iblock"))
	return;

if ( !$USER->IsAdmin() )
	LocalRedirect("/");

$res = CIBlockElement::GetByID($_REQUEST["ELEMENT_ID_FIRM"]);	

if($ar_res = $res->GetNext())
{
	$APPLICATION->SetTitle("$ar_res[NAME]"); 
}
else
{
	LocalRedirect("/lk/");
}




?>
<div class="profile-add">
	<span>Список ответственных лиц</span>
	<a class="add-prof" href="#add-user-window">Добавить пользователя</a>
</div>
<?
	$APPLICATION->IncludeComponent("pl:user.list","",
		array(		
			"PAGE_NAVIGATION_TEMPLATE" => "",
			"SET_TITLE" => "N",			
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "0",
			"PAGER_TITLE" => "",
			"PAGER_TEMPLATE" => "",
			"PAGER_SHOW_ALWAYS" => "N",
			"FIELDS" => array('ID','LOGIN','EMAIL','LAST_NAME','SECOND_NAME','NAME','PERSONAL_PROFESSION'),
			"FIELDS_SELECT" => array('UF_FIRM','UF_ACCESS_DIRECTORY'),
			"FILTER" => array("UF_FIRM" => $_REQUEST["ELEMENT_ID_FIRM"])
		)
);?>
<div class="profile-add">
<span>Адрес доставки</span>
<a class="add-deliv-adres" href="#new-address-form">Добавить адрес</a>
</div>
<?$APPLICATION->IncludeComponent(
	"pl:iblock.element.list",
	"addresses",
	Array(
		"FILTER" => array("PROPERTY_FIRM_ID" => $_REQUEST["ELEMENT_ID_FIRM"]),
		"DETAIL_URL" => "",
		"IBLOCK_URL" => "/lk/",
		"PAGER_SHOW_ALWAYS" => "N",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "20",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "firm",
		"IBLOCK_ID" => "2",
		"GROUPS" => array(),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "N",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "",
		"VARIABLE_ALIASES" => Array(),
		"VARIABLE_ALIASES" => Array(
		)
	)
);?>
<div class="pad"></div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>