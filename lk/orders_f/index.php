<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Первичный расчет заявки");

if(!CModule::IncludeModule("iblock"))
	return;

if ( !$USER->IsAdmin() )
	LocalRedirect("/");

$res = CIBlockElement::GetByID($_REQUEST["ELEMENT_ID_FIRM"]);	

if($ar_res = $res->GetNext())
{
	$APPLICATION->SetTitle("$ar_res[NAME]"); 
}
else
{
	LocalRedirect("/lk/");
}

?><?$APPLICATION->IncludeComponent(
	"pl:iblock.element.list",
	"orders_f",
	Array(
		"FILTER" => array("PROPERTY_FIRM_ID"=>$_REQUEST["ELEMENT_ID_FIRM"]),
		"ACCESS_DIRECTORY" => "Y",
		"DETAIL_URL" => "",
		"IBLOCK_URL" => "/lk/orders_f/",
		"PAGER_SHOW_ALWAYS" => "N",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "20",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "firm",
		"IBLOCK_ID" => "4",
		"GROUPS" => array(),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "N",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "",
		"PROPERTY_CODE" => array("CLIENT_STORAGE_IN","CLIENT_BALANCE","NEED","RESERVE","FAGREE","ARTICLE","UNITS","PERIOD","DATE_IN","ITEM_IN","AMOUNT_IN_STORAGE"),
		"VARIABLE_ALIASES" => Array(),
		"VARIABLE_ALIASES" => Array(
		)
	)
);?> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>