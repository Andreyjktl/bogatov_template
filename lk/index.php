<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет администратора");

if ( !$USER->IsAdmin() )
	LocalRedirect("/");

?><?$APPLICATION->IncludeComponent(
	"bitrix:currency.rates",
	"bg_template",
	Array(
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "86400",
		"arrCURRENCY_FROM" => array(0=>"USD",1=>"EUR",),
		"CURRENCY_BASE" => "RUB",
		"RATE_DAY" => "",
		"SHOW_CB" => "Y"
	)
);?><br>
 <a class="add-org" href="#add-org-window">Добавить организацию</a><br>
 <?$APPLICATION->IncludeComponent(
	"pl:iblock.element.list",
	"",
	Array(
		"DETAIL_URL" => "/lk/profiles/#ELEMENT_ID#/",
		"IBLOCK_URL" => "/lk/",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "10",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "firm",
		"IBLOCK_ID" => "1",
		"GROUPS" => array(),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "N",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/lk/",
		"VARIABLE_ALIASES" => Array(),
		"VARIABLE_ALIASES" => Array(
		)
	)
);?> <br>
 <br>
 <?$APPLICATION->IncludeComponent(
	"pl:iblock.element.list",
	"orders_f",
	Array(
		"FILTER" => array("PROPERTY_FIRM_ID"=>$_REQUEST["ELEMENT_ID_FIRM"]),
		"ACCESS_DIRECTORY" => "Y",
		"DETAIL_URL" => "",
		"IBLOCK_URL" => "/lk/orders_f/",
		"PAGER_SHOW_ALWAYS" => "N",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "20",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "firm",
		"IBLOCK_ID" => "4",
		"GROUPS" => array(),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "N",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "",
		"PROPERTY_CODE" => array("CLIENT_STORAGE_IN","CLIENT_BALANCE","NEED","RESERVE","FAGREE","ARTICLE","UNITS","PERIOD","DATE_IN","ITEM_IN","AMOUNT_IN_STORAGE"),
		"VARIABLE_ALIASES" => Array(),
		"VARIABLE_ALIASES" => Array(
		)
	)
);?> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>