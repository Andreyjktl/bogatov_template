<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

$action = $_POST['action'] ? htmlspecialchars(strip_tags( $_POST['action'] )) : "" ;
$firm = $_POST['firm_id'] ? htmlspecialchars(strip_tags( $_POST['firm_id'] )) : "" ;

$arResult = array(
	"error" => true,
	"content" => "",
	"msg" => ""
);

$messRowUpdate = "Запись обновлена (#%s).";
$messRowDelete = "Запись удалена (#%s).";
$messRowAdd = "Запись добавлена (#%s).";

if( !$USER->IsAuthorized())
{
	echo json_encode($arResult["msg"] = "нет доступа");
	exit;
}

if (CModule::IncludeModule("iblock"))
{

	if ( $action == "update" )
	{		
		
		$client_value = $_POST['client_value'] ?  $_POST['client_value']  : "" ;
		$storage_in = $_POST['storage_in'] ?  $_POST['storage_in']  : "" ;
		$client_value = $_POST['client_value'] ?  $_POST['client_value']  : "" ;
		$need = $_POST['need'] ?  $_POST['need']  : "" ;
		$reserve = $_POST['reserve'] ?  $_POST['reserve']  : "" ;
		$fagree = $_POST['fagree'] ?  $_POST['fagree']  : 0 ;
		$check = $_POST['check'] ? $_POST['check'] : "" ;

		$el = new CIBlockElement;
		
		if ( !empty($need) )
		{					
			foreach ( $need as $key => $item )
			{
				$res = CIBlockElement::GetProperty(4, $key);	
				$PROP = array();
				while ($ob = $res->GetNext())
				{
					$PROP[ $ob['CODE'] ] = $ob['VALUE'];
				}	

				$arFields = Array(
				  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
				  "IBLOCK_SECTION" => false,          // элемент лежит в корне раздела
				  "PROPERTY_VALUES" => array(
				  	"STORAGE_IN"=> htmlspecialchars(strip_tags($storage_in[$key])),
				  	"CLIENT_BALANCE"=> htmlspecialchars(strip_tags($client_value[$key])),	
					"NEED"=> htmlspecialchars(strip_tags($need[$key])),		  
					"RESERVE" => htmlspecialchars(strip_tags($reserve[$key])),
					"FAGREE" => htmlspecialchars(strip_tags($fagree[$key])),
					"ARTICLE" => $PROP['ARTICLE'],
					"UNITS" =>  $PROP['UNITS'],
					"FIRM_ID" =>  $PROP['FIRM_ID'],
					"PRODUCT_XML_ID" => $PROP["PRODUCT_XML_ID"],
					"FIRM_XML_ID" => $PROP["FIRM_XML_ID"],
					"PERIOD" => $PROP['PERIOD'],
					"DATE_IN" => $PROP['DATE_IN'],
					"ITEM_IN" => $PROP['ITEM_IN'],
					"AMOUNT_IN_STORAGE" => $PROP['AMOUNT_IN_STORAGE'],

				  )			 
				);

				$PRODUCT_ID = $key; 				

				if($el->Update($PRODUCT_ID, $arFields))
				{
					$arResult['error'] = false;
					$arResult['msg'][] = sprintf($messRowUpdate, $PRODUCT_ID); 	
				}
				else 
				{				
					$arResult['error'] = true;
					$arResult['msg'][] = $el->LAST_ERROR . " (#$PRODUCT_ID)";	
				}

				ob_start();

				$APPLICATION->IncludeComponent(
					"pl:iblock.element.list",
					"ajax_articules",
					Array(
						"FILTER" => array("PROPERTY_FIRM_ID" => $firm),
						"DETAIL_URL" => "",
						"IBLOCK_URL" => "/lk/articules/",
						"PAGER_SHOW_ALWAYS" => "N",
						"EDIT_URL" => "",
						"NAV_ON_PAGE" => "20",
						"MAX_USER_ENTRIES" => "100000",
						"IBLOCK_TYPE" => "firm",
						"IBLOCK_ID" => "4",
						"GROUPS" => array(),
						"STATUS" => "ANY",
						"ELEMENT_ASSOC" => "N",
						"ALLOW_EDIT" => "Y",
						"ALLOW_DELETE" => "Y",
						"SEF_MODE" => "Y",
						"SEF_FOLDER" => "",
						"PROPERTY_CODE" => array("STORAGE_IN","CLIENT_BALANCE", "NEED", "RESERVE", "FAGREE", "ARTICLE", "UNITS", "PERIOD", "DATE_IN", "ITEM_IN", "AMOUNT_IN_STORAGE"),


						"VARIABLE_ALIASES" => Array(),
						"VARIABLE_ALIASES" => Array(
						)
					)
				);

				$outIncludeComponent = ob_get_contents();
				ob_end_clean();

				$arResult['content'] = $outIncludeComponent; 

			}
		}
		// Удаление элементов инфоблока
		if(!empty($check))
		{
			if (is_array($check))
			{
				foreach ( $check as $ID => $item )
				{	
					$DB->StartTransaction();
					if(!CIBlockElement::Delete($ID))
					{
						$strWarning .= 'Error!';
						$DB->Rollback();
						$arResult['error'] = true;	
						$arResult['msg'][] = "Ошибка удаления записи (#$ID)"; 
					}
					else
					{
						$DB->Commit();
						$arResult['error'] = false;	
						$arResult['msg'][] = sprintf($messRowDelete, $ID);	
					}
					
				}
				
				if ( !$arResult['error'] )
				{				
					ob_start();

					$APPLICATION->IncludeComponent(
						"pl:iblock.element.list",
						"ajax_articules",
						Array(
							"FILTER" => array("PROPERTY_FIRM_ID" => $firm),
							"DETAIL_URL" => "",
							"IBLOCK_URL" => "/lk/articules/",
							"PAGER_SHOW_ALWAYS" => "N",
							"EDIT_URL" => "",
							"NAV_ON_PAGE" => "20",
							"MAX_USER_ENTRIES" => "100000",
							"IBLOCK_TYPE" => "firm",
							"IBLOCK_ID" => "4",
							"GROUPS" => array(),
							"STATUS" => "ANY",
							"ELEMENT_ASSOC" => "N",
							"ALLOW_EDIT" => "Y",
							"ALLOW_DELETE" => "Y",
							"SEF_MODE" => "Y",
							"SEF_FOLDER" => "",
							"PROPERTY_CODE" => array("STORAGE_IN","CLIENT_BALANCE", "NEED", "RESERVE", "FAGREE", "ARTICLE", "UNITS", "PERIOD", "DATE_IN", "ITEM_IN", "AMOUNT_IN_STORAGE"),
							"VARIABLE_ALIASES" => Array(),
							"VARIABLE_ALIASES" => Array(
							)
						)
					);

					$outIncludeComponent = ob_get_contents();
					ob_end_clean();

					$arResult['content'] = $outIncludeComponent; 
				}//if ( !$arResult['error'] )			
			}//if (is_array($check))	
		}//if(!empty($check))
	}
	
	if ( $action == "add" )
	{	
		CModule::IncludeModule("catalog");
		
		$check = $_POST['check'] ? $_POST['check'] : "" ;			
		
		$el = new CIBlockElement;
		
		if (is_array($check))
		{					
			foreach ( $check as $productID => $item )
			{				
				
				//Получения основных полей елемента
				$res = CIBlockElement::GetByID($productID);
				if($ar_res = $res->GetNext())
				{				
				  $NAME = $ar_res["~NAME"];
				  $PRODUCT_XML_ID = $ar_res["XML_ID"];
				}			
				//Получение единици измерения
				$res = CIBlockElement::GetProperty(3, $productID);
				$resCatalogProduct = CCatalogProduct::GetByID($productID);
				
				$unit = "";
				if(  CModule::IncludeModule("catalog") ) {
						$res_measure = CCatalogMeasure::getList();
						while($measure = $res_measure->Fetch()) {
							if ($measure['ID'] == $resCatalogProduct['MEASURE'])
								$unit = $measure['SYMBOL_RUS'];
						}        
				}
				//Получение свойств
				$PROP = array();
				while ($ob = $res->GetNext())
				{
					$PROP[ $ob['CODE'] ] = $ob['VALUE'];
				}	
				
				if ( empty($firm) )
				{
					//Получение Внешнего кода организации
					$rsUser = CUser::GetByID( CUser::GetID() );
					$arUser = $rsUser->Fetch();
					$FIRM_ID = $arUser['UF_FIRM'];
				}
				else 
					$FIRM_ID = $firm;
				
				$res = CIBlockElement::GetByID($FIRM_ID);

				if($ar_res = $res->GetNext())					
					$FIRM_XML_ID = $ar_res['XML_ID'];

				$arFields = Array(
					"MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
					"NAME" => $NAME,					
					"IBLOCK_ID" => 4,
					"IBLOCK_SECTION" => false, // элемент лежит в корне раздела
					"PROPERTY_VALUES" => array(
						"NEED" => "0",
						"STORAGE_IN" => "0",
						"CLIENT_BALANCE" => "0",
						"RESERVE" => "0",
						"FAGREE" => "0",

						"ARTICLE" => $PROP['ARTICLE'],
						"UNITS" => $unit,
						"FIRM_ID" => $firm,
						"PRODUCT_XML_ID" => $PRODUCT_XML_ID,
						"FIRM_XML_ID" => $FIRM_XML_ID
					)
				);

				if($ID = $el->Add($arFields))
				{
					$arResult['error'] = false;
					$arResult['msg'][] = sprintf($messRowAdd, $ID);
				}
				else 
				{				
					$arResult['error'] = true;
					$arResult['msg'][] = $el->LAST_ERROR;	
				}
				
				if ( !$arResult['error'] )
				{				
					ob_start();

					$APPLICATION->IncludeComponent(
						"pl:iblock.element.list",
						"ajax_articules",
						Array(
							"FILTER" => array("PROPERTY_FIRM_ID" => $firm),
							"DETAIL_URL" => "",
							"IBLOCK_URL" => "/lk/articules/",
							"PAGER_SHOW_ALWAYS" => "N",
							"EDIT_URL" => "",
							"NAV_ON_PAGE" => "20",
							"MAX_USER_ENTRIES" => "100000",
							"IBLOCK_TYPE" => "firm",
							"IBLOCK_ID" => "4",
							"GROUPS" => array(),
							"STATUS" => "ANY",
							"ELEMENT_ASSOC" => "N",
							"ALLOW_EDIT" => "Y",
							"ALLOW_DELETE" => "Y",
							"SEF_MODE" => "Y",
							"SEF_FOLDER" => "",
							"PROPERTY_CODE" => array("STORAGE_IN","CLIENT_BALANCE", "NEED", "RESERVE", "FAGREE", "ARTICLE", "UNITS", "PERIOD", "DATE_IN", "ITEM_IN", "AMOUNT_IN_STORAGE"),
							"VARIABLE_ALIASES" => Array(),
							"VARIABLE_ALIASES" => Array(
							)
						)
					);

					$outIncludeComponent = ob_get_contents();
					ob_end_clean();

					$arResult['content'] = $outIncludeComponent; 
				}//if ( !$arResult['error'] )					
			}			
		}		
	}	
	
	echo json_encode($arResult);
}
?>