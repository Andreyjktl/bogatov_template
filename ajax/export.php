<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

//if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

if (!$USER->IsAdmin() ) {
	echo json_encode($arResult["msg"] = "нет доступа");
	exit;
}

//Получение Справоkчной Информации текущего пользователя
//Формирование фильтра по артикулу
CModule::IncludeModule("iblock");

$fp = fopen("php://output", "ab");
$arParams["IBLOCK_ID"] = 4;
$obExport = new CIBlockCMLExport;
$PROPERTY_MAP = array();
$SECTION_MAP = array();
$start_time = time();
$arParams["INTERVAL"] = 0;
$arParams["ELEMENTS_PER_STEP"] = 1000;

@set_time_limit(0);

ob_start();
$obExport->Init(
		$fp, 
		$arParams["IBLOCK_ID"], 
		1, 
		false, 
		$work_dir = false, 
		$file_dir = false, 
		$bCheckPermissions = false
);

$obExport->StartExport();
$obExport->StartExportMetadata();
if ($obExport->Init(
	$fp, 
	$arParams["IBLOCK_ID"], 
	2, 
	false, 
	$work_dir = false, 
	$file_dir = false, 
	$bCheckPermissions = false
	))
{
	$obExport->NotCatalog();
	$obExport->ExportFileAsURL();

	
	$obExport->ExportProperties($PROPERTY_MAP);
	$obExport->ExportSections($SECTION_MAP,	0,0 );
	
	$obExport->StartExportCatalog();
	$result = $obExport->ExportElements(
			$PROPERTY_MAP, 
			$SECTION_MAP, 
			$start_time, 
			$arParams["INTERVAL"], 
			$arParams["ELEMENTS_PER_STEP"]
	);


	if ($result) 
	{		
		$obExport->EndExportCatalog();
		$obExport->EndExportMetadata();
		$obExport->EndExport();
	}
}
$c = ob_get_contents();
ob_end_clean();

header('Content-Description: File Transfer');
header("Content-type: text/xml");
header('Content-Disposition: attachment; filename=' . basename("export.xml"));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . strlen($c));
echo $c;
exit;
?>