<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

$action = $_POST['action'] ? htmlspecialchars(strip_tags( $_POST['action'] )) : "" ;
$firm = $_POST['firm_id'] ? htmlspecialchars(strip_tags( $_POST['firm_id'] )) : "" ;

if ( !$USER->IsAdmin() )
{
	echo json_encode($arResult["msg"] = "нет доступа");
	exit;
}

$messRowUpdate = "Запись обновлена (#%s).";
$messRowDelete = "Запись удалена (#%s).";
$messRowAdd = "Запись добавлена (#%s).";

if ( $action == "add" )
{
	$address = $_POST['address'] ? strip_tags( $_POST['address'] ) : "" ;
	$address = htmlspecialchars( $address );

	if (CModule::IncludeModule("iblock"))
	{
		$el = new CIBlockElement;

		$PROP = array(
			1 => $firm
		);	

		$arResult = array(
			"error" => true,
			"content" => ""
		);

		if( empty($address))
		{
			$arResult['error'] = true;
			$arResult['el'][] = "new-address";
		}

		$arArray = Array(
		  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
		  "IBLOCK_SECTION_ID" => false,       // элемент лежит в корне раздела
		  "IBLOCK_ID"      => 2,
		  "PROPERTY_VALUES"=> $PROP,
		  "NAME"           => $address,
		  "ACTIVE"         => "Y",            // активен
		  "PREVIEW_TEXT"   => "",
		  "DETAIL_TEXT"    => ""	
		  );

		if($PID = $el->Add($arArray))
		{
			ob_start();
			$APPLICATION->IncludeComponent(
				"pl:iblock.element.list",
				"ajax_addresses_list",
				Array(
					"FILTER" => array("PROPERTY_FIRM_ID" => $firm),
					"DETAIL_URL" => "",
					"PAGER_SHOW_ALWAYS" => "N",
					"IBLOCK_URL" => "/lk/",
					"EDIT_URL" => "",
					"NAV_ON_PAGE" => "20",
					"MAX_USER_ENTRIES" => "100000",
					"IBLOCK_TYPE" => "firm",
					"IBLOCK_ID" => "2",
					"GROUPS" => array(),
					"STATUS" => "ANY",
					"ELEMENT_ASSOC" => "N",
					"ALLOW_EDIT" => "Y",
					"ALLOW_DELETE" => "Y",
					"SEF_MODE" => "Y",
					"SEF_FOLDER" => "",
					"VARIABLE_ALIASES" => Array(),
					"VARIABLE_ALIASES" => Array(
					)
				)
			);
			$outIncludeComponent = ob_get_contents();
			ob_end_clean();

			if ( !empty($outIncludeComponent) )
			{
				$arResult['error'] = false;
				$arResult['content'] = $outIncludeComponent; 
			}
			
			$arResult['msg'] = sprintf($messRowAdd, $PID);  	
		}  
		else
		{		
			$arResult['error'] = true;
			$arResult['msg'][] = $el->LAST_ERROR; 
		}

		echo json_encode($arResult);
	}
}

if ( $action == "delete" )
{
	
	$check = $_POST['check'] ? $_POST['check'] : "" ;
	
	if (CModule::IncludeModule("iblock"))
	{		
		$DB->StartTransaction();
		if(!CIBlockElement::Delete($check))
		{			
			$arResult['error'] = true;
			$arResult['msg'] = "Ошибка удаления записи (#$PID)";
			$DB->Rollback();
		}
		else
		{
			$arResult['error'] = false;
			$arResult['msg'] = sprintf($messRowDelete, $check); 
			$DB->Commit();
			
			ob_start();
			$APPLICATION->IncludeComponent(
				"pl:iblock.element.list",
				"ajax_addresses_list",
				Array(
					"FILTER" => array("PROPERTY_FIRM_ID" => $firm),
					"DETAIL_URL" => "",
					"PAGER_SHOW_ALWAYS" => "N",
					"IBLOCK_URL" => "/lk/",
					"EDIT_URL" => "",
					"NAV_ON_PAGE" => "20",
					"MAX_USER_ENTRIES" => "100000",
					"IBLOCK_TYPE" => "firm",
					"IBLOCK_ID" => "2",
					"GROUPS" => array(),
					"STATUS" => "ANY",
					"ELEMENT_ASSOC" => "N",
					"ALLOW_EDIT" => "Y",
					"ALLOW_DELETE" => "Y",
					"SEF_MODE" => "Y",
					"SEF_FOLDER" => "",
					"VARIABLE_ALIASES" => Array(),
					"VARIABLE_ALIASES" => Array(
					)
				)
			);
			$outIncludeComponent = ob_get_contents();
			ob_end_clean();
			
			$arResult['content'] = $outIncludeComponent;
		}
	}
	
	echo json_encode($arResult);
}
?>