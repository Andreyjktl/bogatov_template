<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

$action = $_POST['action'] ? htmlspecialchars(strip_tags( $_POST['action'] )) : "" ;
$firm = $_POST['firm_id'] ? htmlspecialchars(strip_tags( $_POST['firm_id'] )) : "" ;

$arResult = array(
	"error" => true,
	"content" => "",
	"msg" => ""
);

$messRowUpdate = "Запись обновлена (#%s).";
$messRowDelete = "Запись удалена (#%s).";
$messRowAdd = "Запись добавлена (#%s).";

if ( !$USER->IsAdmin() )
{
	echo json_encode($arResult["msg"] = "нет доступа");
	exit;
}
//Добавление пользователя
if ( $action == "add")
{
	$name = $_POST['name'] ? htmlspecialchars(strip_tags( $_POST['name'] )) : "" ;
	$login = $_POST['login'] ? htmlspecialchars(strip_tags( $_POST['login'] )) : "" ;
	$personal_profession = $_POST['personal_profession'] ? htmlspecialchars(strip_tags( $_POST['personal_profession'] )) : "" ;
	$access_directory = $_POST['access_directory'] ? htmlspecialchars(strip_tags( $_POST['access_directory'] )) : "" ;
	$pass = $_POST['pass'] ? htmlspecialchars(strip_tags( $_POST['pass'] )) : "" ;
	$passv = $_POST['passv'] ? htmlspecialchars(strip_tags( $_POST['passv'] )) : "" ;
	$xml_id = $_POST['xml_id'] ? htmlspecialchars(strip_tags( $_POST['xml_id'] )) : "" ;
	
	$name_user = "";
	$last_name_user = "";
	$second_name_user = "";

	if( !empty($name) )
	{
		$temp = explode(' ', $name);
		$last_name_user = $temp[0];
		$name_user = $temp[1] ? $temp[1] : "";
		$second_name_user = $temp[2] ? $temp[2] : "";
	}
	else
	{
		$arResult['error'] = true;
		$arResult['el'][] = "user-fio";
	}

	if( !empty($login) )
	{

	}
	else
	{
		$arResult['error'] = true;
		$arResult['el'][] = "user-login";
	}

	if( !empty($personal_profession) )
	{

	}
	else
	{
		$arResult['error'] = true;
		$arResult['el'][] = "user-post";
	}

	if( !empty($pass) || !empty($passv) )
	{
		if ( $pass != $passv)
		{
			$arResult['error'] = true;
			$arResult['el'][] = "user-pass-confirm";		
		}

	}
	else
	{
		$arResult['error'] = true;
		$arResult['el'][] = "user-pass";
	}


	$user = new CUser;
	$arFields = Array(
		"SECOND_NAME" => $second_name_user,
		"NAME" => $name_user,
		"LAST_NAME" => $last_name_user,
		"EMAIL" => "ivanov@microsoft.com",
		"LOGIN" => $login,
		"LID" => "ru",
		"ACTIVE" => "Y",
		"XML_ID" => $xml_id,
		"GROUP_ID" => array(5),
		"PASSWORD" => $pass,
		"CONFIRM_PASSWORD" => $passv,
		"PERSONAL_PROFESSION" => $personal_profession,
		"UF_ACCESS_DIRECTORY" => $access_directory,
		"UF_FIRM" => $firm
	);

	$ID = $user->Add($arFields);
	if (intval($ID) > 0)
	{	
		ob_start();
		$APPLICATION->IncludeComponent("pl:user.list","ajax_users_list",
			array(		
				"PAGE_NAVIGATION_TEMPLATE" => "",
				"SET_TITLE" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0",
				"PAGER_TITLE" => "",
				"PAGER_TEMPLATE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"FIELDS" => array('ID','LOGIN','EMAIL','LAST_NAME','SECOND_NAME','NAME','PERSONAL_PROFESSION'),
				"FIELDS_SELECT" => array('UF_FIRM','UF_ACCESS_DIRECTORY'),
				"FILTER" => array("UF_FIRM" => $firm)
			)
		);
		$outIncludeComponent = ob_get_contents();
		ob_end_clean();		
		$arResult['error'] = false;
		$arResult['msg'] = sprintf($messRowAdd, $ID);  
		$arResult['content'] = $outIncludeComponent; 
	}
	else
	{   
		$arResult['error'] = true;
		$arResult['msg'][] = $user->LAST_ERROR; 
	}

	echo json_encode($arResult);
}

//Редактирование пользователя
if ( $action == "update")
{
	$name = $_POST['name'] ? $_POST['name'] : "" ;
	$check = $_POST['check'] ? $_POST['check'] : "" ;
	$login = $_POST['login'] ? $_POST['login'] : "" ;
	$personal_profession = $_POST['personal_profession'] ? $_POST['personal_profession'] : "" ;
	$access_directory = $_POST['access_directory'] ? $_POST['access_directory'] : "" ;	
	
	//Обновление записей
	if ( !empty($name) )
	{
		if (is_array($name))
		{
			//$messRowUpdate = count($name) > 1 ? "Записи обновлены" : $messRowUpdate ;
			
			$user = new CUser;		

			foreach ( $name as $ID => $item_name )
			{

				$temp = explode(' ', $item_name);
				$last_name_user = $temp[0];
				$name_user = $temp[1] ? $temp[1] : "";
				$second_name_user = $temp[2] ? $temp[2] : "";			

				$arFields = Array(
					"SECOND_NAME" => $second_name_user,
					"NAME" => $name_user,
					"LAST_NAME" => $last_name_user,
					"EMAIL" => "ivanov@microsoft.com",
					"LOGIN" => htmlspecialchars(strip_tags( $login[$ID] )),				
					"PERSONAL_PROFESSION" => htmlspecialchars(strip_tags( $personal_profession[$ID] )),
					"UF_ACCESS_DIRECTORY" => htmlspecialchars(strip_tags( $access_directory[$ID] )),				
				);				
				

				$user->Update($ID, $arFields);
				if ($user->LAST_ERROR)
				{
					$arResult['error'] = true;
					$arResult['msg'][] = $user->LAST_ERROR . " (#$ID)";
				}
				else
				{
					$arResult['error'] = false;
					$arResult['msg'][] = sprintf($messRowUpdate, $ID);
				}
			}

			if ($arResult['error'])
			{
				;
			}
			else 
			{
				ob_start();
				$APPLICATION->IncludeComponent("pl:user.list","ajax_users_list",
					array(		
						"PAGE_NAVIGATION_TEMPLATE" => "",
						"SET_TITLE" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "0",
						"PAGER_TITLE" => "",
						"PAGER_TEMPLATE" => "",
						"PAGER_SHOW_ALWAYS" => "N",
						"FIELDS" => array('ID','LOGIN','EMAIL','LAST_NAME','SECOND_NAME','NAME','PERSONAL_PROFESSION'),
						"FIELDS_SELECT" => array('UF_FIRM','UF_ACCESS_DIRECTORY'),
						"FILTER" => array("UF_FIRM" => $firm)
					)
				);
				$outIncludeComponent = ob_get_contents();
				ob_end_clean();

				$arResult['content'] = $outIncludeComponent; 			
			}
		}
	}
	
	//Удаление пользователей
	if(!empty($check))
	{
		if (is_array($check))
		{
			//$messRowDelete = count($check) > 1 ? "Записи удалены" : $messRowDelete ;
			
			$user = new CUser;		

			foreach ( $check as $ID => $item )
			{		
				if ( CUser::Delete($ID) )
					$arResult['msg'][] = sprintf($messRowDelete, $ID); 	
				else
				{
					$arResult['error'] = true;	
					$arResult['msg'][] = "Ошибка при удалении пользователя. Возможно он связан с другими объектами (#$ID).";			
				}
			}
		}

		$arResult['error'] = false;		

		ob_start();
		$APPLICATION->IncludeComponent("pl:user.list","ajax_users_list",
			array(		
				"PAGE_NAVIGATION_TEMPLATE" => "",
				"SET_TITLE" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "0",
				"PAGER_TITLE" => "",
				"PAGER_TEMPLATE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"FIELDS" => array('ID','LOGIN','EMAIL','LAST_NAME','SECOND_NAME','NAME','PERSONAL_PROFESSION'),
				"FIELDS_SELECT" => array('UF_FIRM','UF_ACCESS_DIRECTORY'),
				"FILTER" => array("UF_FIRM" => $firm)
			)
		);
		$outIncludeComponent = ob_get_contents();
		ob_end_clean();

		$arResult['content'] = $outIncludeComponent; 
		
		
	}	
	echo json_encode($arResult);	
}

if ( $action == "pass")
{
	$pass = $_POST['pass'] ? htmlspecialchars(strip_tags( $_POST['pass'] )) : "" ;
	$passv = $_POST['passv'] ? htmlspecialchars(strip_tags( $_POST['passv'] )) : "" ;
	$ID = $_POST['user_id'] ? htmlspecialchars(strip_tags( $_POST['user_id'] )) : "" ;
	
	if( !empty($pass) || !empty($passv) )
	{
		if ( $pass != $passv)
		{
			$arResult['error'] = true;
			$arResult['el'][] = "user-pass-confirm";			
		}

	}
	else
	{
		$arResult['error'] = true;
		$arResult['el'][] = "user-pass";
	}
	
	$user = new CUser;
	$arFields = Array(		
		"PASSWORD" => $pass,
		"CONFIRM_PASSWORD" => $passv,	
	);
	
	$user->Update($ID, $arFields);	
	
	if ($user->LAST_ERROR)
	{
		$arResult['error'] = true;
		$arResult['msg'][] = $user->LAST_ERROR . " (#$ID)";
	}
	else 
	{
		$arResult['error'] = false;
		$arResult['msg'] = "Пароль пользователя был изменен (#$ID)";		
	}
	
	echo json_encode($arResult);
}

?>