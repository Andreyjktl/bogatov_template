<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

$name = $_POST['org-name'] ? strip_tags( $_POST['org-name'] ) : "" ;
$xml_id = $_POST['org-xml-id'] ? strip_tags( $_POST['org-xml-id'] ) : "" ;
$name = htmlspecialchars( $name );

if ( !$USER->IsAdmin() )
{
	echo json_encode($arResult["msg"] = "нет доступа");
	exit;
}

if (CModule::IncludeModule("iblock"))
{
	$el = new CIBlockElement;

	$PROP = array();	
	
	$arResult = array(
		"error" => true,
		"content" => ""
	);
	
	$messRowUpdate = "Запись обновлена (#%s).";
	$messRowDelete = "Запись удалена (#%s).";
	$messRowAdd = "Запись добавлена (#%s).";
	
	$arArray = Array(
	  "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
	  "IBLOCK_SECTION_ID" => false,       // элемент лежит в корне раздела
	  "IBLOCK_ID"      => 1,
	  "PROPERTY_VALUES"=> $PROP,
	  "NAME"           => $name,
	  "XML_ID"         => $xml_id,
	  "ACTIVE"         => "Y",            // активен
	  "PREVIEW_TEXT"   => "",
	  "DETAIL_TEXT"    => "",
	  //"DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/image.gif")
	  );

	if($PID = $el->Add($arArray))
	{
		ob_start();
		$APPLICATION->IncludeComponent(
			"pl:iblock.element.list",
			"ajax_firms_list",
			Array(
				"DETAIL_URL" => "/lk/profiles/#ELEMENT_ID#/",
				"IBLOCK_URL" => "/lk/",
				"EDIT_URL" => "",
				"NAV_ON_PAGE" => "10",
				"PAGER_SHOW_ALWAYS" => "N",
				"MAX_USER_ENTRIES" => "100000",
				"IBLOCK_TYPE" => "firm",
				"IBLOCK_ID" => "1",
				"GROUPS" => array(),
				"STATUS" => "ANY",
				"ELEMENT_ASSOC" => "N",
				"ALLOW_EDIT" => "Y",
				"ALLOW_DELETE" => "Y",
				"SEF_MODE" => "Y",
				"SEF_FOLDER" => "/lk/",
				"VARIABLE_ALIASES" => Array(),
				"VARIABLE_ALIASES" => Array(
				)
			)
		);		
		$outIncludeComponent = ob_get_contents();
		ob_end_clean();
		
		if ( !empty($outIncludeComponent) )
		{
			$arResult['error'] = false;
			$arResult['content'] = $outIncludeComponent; 
			$arResult['msg'] = sprintf($messRowAdd, $PID);
		}
	}  
	else
	{		
		$arResult['error'] = true;
		$arResult['msg'] = $el->LAST_ERROR; 
	}
	
	echo json_encode($arResult);

}
?>