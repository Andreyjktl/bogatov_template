BX.ready(function() {

    $('.basket-edit-count').keyup(function(){
        var $balance = +$(this).attr('data-balance');
        var $product = +$(this).attr('data-product');
        var $val = +$(this).val();

        if ( $balance >=  $val)
        {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/ajax/basket.php',
                data: {
                    action: "update_to_basket",
                    product: $product,
                    number: $val,                
                },
                success: function(res){	                   
                    for ( var key in res.items )
                    {
                        $('.ajax-item-sum-' + key).text( res.items[key].SUMF );                       
                    } 
                    $('.ajax-total-sum').text( res.basket['TOTAL_PRICE'] );
                    $('.ajax-basket-line').html( res.basket_line ); 
                }
             }); 
        }
        else
        {
             $(this).val($balance);
        }
    })

    $('.basket-edit-count').change(function(){

        var $val = +$(this).val();
        if ( $val < 1 )
            $(this).val(1);
    })
    
    $('.delete-item-basket').click( function(){       
          var $product = +$(this).attr('data-product');             
          $.confirm({
            title:"Удаление",
            text:"Действительно желаете удалить товар?",
            confirm: function(button) {               
                
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/ajax/basket.php',
                    data: {
                        action: "delete_to_basket",
                        product: $product,                                  
                    },
                    success: function(res){	                   
                        $('.item-' + $product).remove();
                        $('.ajax-total-sum').text( res.basket['TOTAL_PRICE'] );
                        $('.ajax-basket-line').html( res.basket_line ); 
                        show_msg_complete(res.msg);

                        if ( res.render )                       
                            setTimeout(function(){  window.location.href =  res.render; }, 2000);                        
                    }
                 });       
            },
            cancel: function(button) {
              
            },
            confirmButton: "Да",
            cancelButton: "Нет",   
        });
        
    })
});