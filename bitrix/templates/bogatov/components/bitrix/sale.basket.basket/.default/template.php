<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? //var_dump($arResult["GRID"]["HEADERS"]); ?>
<? if( count($arResult["GRID"]["ROWS"]) > 0 ) : ?>

<? // Вытаскиваем свойства товара по ID ?>
<? /*  $db_props = CIBlockElement::GetProperty("3", $arItem["PRODUCT_ID"], "sort", "asc", array());
$PROPS = array();
while($ar_props = $db_props->Fetch())
$PROPS[$ar_props['CODE']] = $ar_props['VALUE']; */?>
<? // Отображение Props ?>
<? //echo "<pre>"; var_dump($PROPS); echo "</pre>"; ?>


<div class="items">
	<div class="items-head">
		<div class="head-row">		
			<div class="nomenclature">Номенклатура</div>
			<div class="measure">Ед.изм-я</div>
			<div class="balance">Доступный остаток</div>
			<div class="order-current">Тек-я заявка (количество)</div>
			<div class="price">Цена (Валютная)</div>
			<div class="price">Цена</div>
			<div class="sum">Сумма(Валютная)</div>
			<div class="sum">Сумма</div>
			<div class="del-item"></div>
		</div>
	</div>
	<div class="items-list">
	<? //echo "<pre>"; var_dump($arResult); echo "</pre>"; ?>
	<? foreach ($arResult["GRID"]["ROWS"] as $k => $arItem): ?>

	<? /* $db_props = CIBlockElement::GetProperty("3", $arItem["PRODUCT_ID"], "sort", "asc", array());
$PROPS = array();
while($ar_props = $db_props->Fetch())
$PROPS[$ar_props['CODE']] = $ar_props['VALUE']; */ ?>
		
		<?  // вывод доступных свойств ?>
		<?  //echo "<pre>"; var_dump($arItem); echo "</pre>"; ?>
		<?  //echo "<pre>"; var_dump($PROPS); echo "</pre>"; ?>

		<div class="item item-<?= $arItem["ID"] ?>">
			<div class="nomenclature"><?= $arItem["NAME"]?></div>
			<div class="measure"><?= $arItem["MEASURE_TEXT"] ?></div>
			<div class="balance"><?= $arItem["AVAILABLE_QUANTITY"] ? number_format($arItem["AVAILABLE_QUANTITY"],3,".","") : 0 ?></div>
			<div class="order-current">
				<input data-product="<?= $arItem["ID"] ?>" data-balance="<?= $arItem["AVAILABLE_QUANTITY"] ?>" value="<?= $arItem["~QUANTITY"] ? $arItem["~QUANTITY"] : 1 ?>" class="edit-num basket-edit-count " type="text" >
			</div>
			<div class="price"> <?=$arItem["PROPERTY_PRICEVAL_VALUE"]?><?= " "?><?=$arItem["PROPERTY_VALUTA_VALUE"]?></div>
			<div class="price"><?= $arPrice["PRINT_VALUE"] ?><?=$arItem["PRICE"]?></div>
			<div class="priceval"><?= $arItem["PROPERTY_PRICEVAL_VALUE"]*$arItem["QUANTITY"] ?><?= " "?><?=$arItem["PROPERTY_VALUTA_VALUE"]?></div>
			<div class="sum ajax-item-sum-<?= $arItem["ID"] ?>"><?= $arItem["SUM"] ?></div>
			<div class="del-item"><a data-product="<?= $arItem["ID"] ?>" class="delete-item-basket" ></a></div>
		</div>		
	<? endforeach; ?>
	</div>
</div>
<div class="basket-total">	
	<a href="/profile/catalog/" class="add-basket-button">Добавить  позицию в заявку</a>
	Итоговая сумма: <span class="ajax-total-sum"><?= number_format($arResult["allSum"], 2, ".", " "); //CurrencyFormat($arResult["allSum"], 'RUB'); ?></span><img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/ruble-orange.gif" class="ruble-orange-img" />
</div>
<? else: ?>
<br>
<br>
<p style="text-align: center"><?= GetMessage("SALE_NO_ITEMS") ?></p>
<? endif ?>
			
