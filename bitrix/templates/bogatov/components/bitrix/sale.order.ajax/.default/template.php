<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
	if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
	{
		if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
		{
			if(strlen($arResult["REDIRECT_URL"]) > 0)
			{
				$APPLICATION->RestartBuffer();
				?>
				<script type="text/javascript">
					window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
				</script>
				<?
				die();
			}

		}
	}
?>

<? if (!empty($arResult["ORDER"])) : ?>
<?;// OnOrderAddHandler($arResult["ORDER"]["ID"]); ?>
<? LocalRedirect($arParams["PATH_TO_PERSONAL"] . "?ORDER_ID=" . $arResult["ORDER"]["ID"]);	 ?>
<? else : ?>	

<? if($_POST["is_ajax_post"] != "Y") : ?>
	<div id="basket_detail" class="basket-detail">

		<form id="valid" action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
			<?=bitrix_sessid_post()?>
			<?php
				if(IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0)
				{
					//for IE 8, problems with input hidden after ajax
				?>
				<span style="display:none;">
				<input type="text" name="PERSON_TYPE" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>" />
				<input type="text" name="PERSON_TYPE_OLD" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>" />
				</span>
					<?
				}
				else
				{
					foreach($arResult["PERSON_TYPE"] as $v)
					{
						?>
						<input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?=$v["ID"]?>" />
						<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$v["ID"]?>" />
						<?
					}
				}

			?>
			<div id="order_form_content">
				<?	
					include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
				?>		
				<div class="formDiv">	
					<label for="comment"><?=GetMessage("SOA_TEMPL_SUM_COMMENTS")?></label>
					<textarea name="COMMENTS" id="comment" rows="10" cols="45" name="text" ><?=$arResult["USER_VALS"]["COMMENTS"]?></textarea>
				</div>		
				<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
				<input type="hidden" name="profile_change" id="profile_change" value="N">
				<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
				
				<div id="ajas_system_msg">
<? endif ?>
				<?
					if(!empty($arResult["ERROR"]))
					{
						foreach($arResult["ERROR"] as $v)
							echo ShowError($v);
					}
					elseif(!empty($arResult["OK_MESSAGE"]))
					{
						foreach($arResult["OK_MESSAGE"] as $v)
							echo ShowNote($v);
					}
				?>	
					
<? if($_POST["is_ajax_post"] != "Y") : ?>					
				</div>
				<div class="formDiv">
					<button onclick="submitForm('Y')" class="sub-basket"><?=GetMessage("SOA_TEMPL_BUTTON")?></button>
				</div>	
			</div>
		</form>
	</div>

	<script>

	$(document).ready(function() {
		$(".shipping").datepicker({
				dateFormat: 'dd/mm/yy',
				minDate: new Date(), // = today
				showOn: "button",
				buttonImage: "<?= SITE_TEMPLATE_PATH ?>/assets/images/calendar-icon.png",
				buttonImageOnly: true
		});
		if ($(".shipping").length) {
				$(".shipping").mask('99/99/9999');
		}	
	});
	</script>	
	<? endif ?>
<? endif ?>