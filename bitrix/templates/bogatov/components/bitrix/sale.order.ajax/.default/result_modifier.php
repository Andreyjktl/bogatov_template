<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php
	foreach( $arResult["ORDER_PROP"]["USER_PROPS_N"] as $key => $arProperties )
	{
		if ($arProperties["CODE"] == "ADDRESS")
		{
			
			$arProperties["TYPE"] = "ADDRESS";
			$arProperties["LIST"] = array();
			
			$rsUser = CUser::GetByID( CUser::GetID() );
			$arUser = $rsUser->Fetch();
			$firmID = $arUser['UF_FIRM'];
			
			$res = CIBlockElement::GetList(
					array("SORT"=>"ASC"), 
					array("PROPERTY_FIRM_ID" => $firmID, "ACTIVE"=>"Y", "IBLOCK_CODE"=>"delivery_address"), 
					false,
					false,
					array("NAME")
			);
			
			
			while($ob = $res->GetNextElement())
			{				
				$arProperties["LIST"][] = $ob->fields["NAME"];				
			}
			
			$arResult["ORDER_PROP"]["USER_PROPS_N"][$key] = $arProperties;

		}
		
		if ($arProperties["CODE"] == "FIRM_XML_ID")
		{			
			$rsUser = CUser::GetByID( CUser::GetID() );
			$arUser = $rsUser->Fetch();
			$FIRM_ID = $arUser['UF_FIRM'];
			$res = CIBlockElement::GetByID($FIRM_ID);
			
			if($ar_res = $res->GetNext())					
				$XML_ID = $ar_res['XML_ID'];	
			
			$arProperties["TYPE"] = "FIRM_XML_ID";		
			$arProperties["VALUE"] = $XML_ID;	
			$arResult["ORDER_PROP"]["USER_PROPS_N"][$key] = $arProperties;
			
		}
		
		if ($arProperties["CODE"] == "DATE_SHIPMENT")
		{			
			$arProperties["TYPE"] = "DATE";		
			$arResult["ORDER_PROP"]["USER_PROPS_N"][$key] = $arProperties;
		}
	}
	 
	
?>