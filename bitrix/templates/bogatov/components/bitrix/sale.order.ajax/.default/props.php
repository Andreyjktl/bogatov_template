<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<?php foreach( $arResult["ORDER_PROP"]["USER_PROPS_N"] as $arProperties ) : ?>
<? //var_dump($arProperties);  ?>
<? if ($arProperties["TYPE"] == "TEXT") : ?>
<div class="formDiv">
	<label for="shipping"><?=$arProperties["NAME"]?></label>
	<input type="text" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"  class="" value="<?=$arProperties["VALUE"]?>">
</div>
<? endif ?>
<? if ($arProperties["TYPE"] == "FIRM_XML_ID") : ?>
<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"  class="" value="<?=$arProperties["VALUE"]?>">
<? endif ?>
<? if ($arProperties["TYPE"] == "DATE") : ?>
<div class="formDiv">
	<label for="shipping"><?=$arProperties["NAME"]?></label>
	<input type="text" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"  class="shipping required" value="<?=$arProperties["VALUE"]?>">
</div>
<? endif ?>
<? if ($arProperties["TYPE"] == "ADDRESS") : ?>
<div class="formDiv">
	<label for="delivery"><?=$arProperties["NAME"]?></label>
	<select name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"  class="required">								   
		<? foreach(  $arProperties["LIST"] as $value ) : ?>
		<option value="<?= $value ?>"><?= $value ?></option>
		<? endforeach ?>		
	</select>	
	<a class="showAll"></a>
</div>
<? endif ?>
<?php endforeach ?>
