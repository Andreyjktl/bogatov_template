<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php

	$arProps = array();
	

	if(is_array($arResult["ORDERS"]) && !empty($arResult["ORDERS"]))
		foreach ($arResult["ORDERS"] as $order)
		{
			$stat = $order['ORDER']['CANCELED'] == 'Y' ? 'PSEUDO_CANCELLED' : $order["ORDER"]["STATUS_ID"];
			$arResult["ORDER_BY_STATUS"][$stat][] = $order;
			
		}
	CModule::IncludeModule("sale");
	
	foreach($arResult["ORDER_BY_STATUS"] as $key => $group)
	{
		foreach($group as $k => $order)
		{
			foreach($order['BASKET_ITEMS'] as $b => $basket_item)
			{
				//echo "<pre>"; print_r($basket_item); echo "</pre>" ;
				$arResult["ORDER_BY_STATUS"][$key][$k]["ORDER"]["QUANTITY"]++;				
				$arResult["ORDER_BY_STATUS"][$key][$k]["ORDER"]["MEASURE_TEXT"] =  "шт";
				
			}
			
			$ORDER_ID =  $order["ORDER"]["ACCOUNT_NUMBER"];
			
			$db_vals = CSaleOrderPropsValue::GetList(
				array("SORT" => "ASC"),
				array(
						"ORDER_ID" => $ORDER_ID,						
					)
				);
			while ( $arPropVals = $db_vals->Fetch()) {
				$arResult["ORDER_BY_STATUS"][$key][$k]["ORDER"]["PROP"][] = str_replace("/",".", $arPropVals);
			}					
		}
	}
		
	
?>