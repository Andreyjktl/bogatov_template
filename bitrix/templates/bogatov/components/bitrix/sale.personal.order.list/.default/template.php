<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

<?else:?>

	<?if(!empty($arResult['ERRORS']['NONFATAL'])):?>

		<?foreach($arResult['ERRORS']['NONFATAL'] as $error):?>
			<?=ShowError($error)?>
		<?endforeach?>

	<?endif?>
	<? if (!empty($_REQUEST["ORDER_ID"])) : ?>
	<script> 
		$(document).ready(function() {		
		show_msg_complete('<b><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></b><br/><br /><table class="sale_order_full_table"><tr><td><?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => date('d.m.Y'), "#ORDER_ID#" => $_REQUEST["ORDER_ID"]))?></td></tr></table>',2000); 	
		
		})		
	</script>
	<? endif ?>

	<?if(!empty($arResult['ORDERS'])):?>
	<table class="items">
		<thead class="items-head">
			<tr class="head-row">

					<td class="order-num">№ заказа</td>
					<td class="order-date">Дата заказа</td>
					<td class="order-adress">Адрес доставки</td>
					<td class="delivery-date">Дата доставки</td> 
					<td class="num-items">Кол-во позиций</td>
					<?/*<td class="comment">Комментарии</td>*/?>
					<td class="num-items"></td>
			</tr>	
		</thead>	
		<tbody class="items-list">
		<?foreach($arResult["ORDER_BY_STATUS"] as $key => $group):?>
			<?foreach($group as $k => $order):?>	
			<?// echo "<pre>"; print_r($order["ORDER"]); echo "</pre>" ?>				
			<tr class="item">						
				<td class="order-num"><a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?><?=$order["ORDER"]["ID"]?>/"><?=$order["ORDER"]["ACCOUNT_NUMBER"]?></a></td>
				<td class="order-date"><?=$order["ORDER"]["DATE_INSERT_FORMATED"];?></td>
				<td class="order-adress"><?= $order["ORDER"]["PROP"][0]["VALUE"]?></td>
				<td class="delivery-date"><?= $order["ORDER"]["PROP"][1]["VALUE"]?></td>  
				<td class="num-items"><?= $order["ORDER"]["QUANTITY"] ?> <?= $order["ORDER"]["MEASURE_TEXT"] ?>.</td>
				<?/*<td class="comment"><?= $order["ORDER"]['USER_DESCRIPTION'] ?></td>*/?>
				<td class="num-items"><a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?><?=$order["ORDER"]["ID"]?>/"><?= GetMessage("SPOL_ORDER_DETAIL")?></a></td>
			</tr>			
			<?endforeach?>		
		<?endforeach?>
		</tbody>
	</table>

	<?if(strlen($arResult['NAV_STRING'])):?>
		<?=$arResult['NAV_STRING']?>
	<?endif?>

	<?else:?>
		<br>
		<br>
		<p style="text-align: center"><?=GetMessage('SPOL_NO_ORDERS')?></p>
	<?endif?>
	

<?endif?>