<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

<?else:?>

	<?if(!empty($arResult['ERRORS']['NONFATAL'])):?>

		<?foreach($arResult['ERRORS']['NONFATAL'] as $error):?>
			<?=ShowError($error)?>
		<?endforeach?>

	<?endif?>

	<?if(!empty($arResult['ORDERS'])):?>
	<div class="items">
		<div class="items-head">
			<div class="head-row">
				<a href="">
				<div class="order-num">№ заказа</div>
				<div class="order-date">Дата заказа</div>
				<div class="order-adress">Адрес доставки</div>
				<div class="delivery-date">Дата доставки</div>  
				<div class="num-items">Кол-во позиций</div>
				<div class="comment">Комментарии</div>
				</a>
			</div>	
		</div>
		<div class="items-list">
		<?foreach($arResult["ORDER_BY_STATUS"] as $key => $group):?>
			<?foreach($group as $k => $order):?>	
			<?// echo "<pre>"; print_r($order["ORDER"]); echo "</pre>" ?>			
			<div class="item">	
				<a href="<?=$order["ORDER"]["URL_TO_DETAIL"]?><?=$order["ORDER"]["ID"]?>/">
				<div class="order-num"><?=$order["ORDER"]["ACCOUNT_NUMBER"]?></div>
				<div class="order-date"><?=$order["ORDER"]["DATE_INSERT_FORMATED"];?></div>
				<?	$i = 1; foreach( $order["ORDER"]["PROP"] as $prop ) : ?>
				<div class="<?= $i == 1 ? "order-adress" : "delivery-date" ?>"><?= $prop["VALUE"]?></div>
				<? $i++; endforeach;  ?>				
				<div class="num-items"><?= $order["ORDER"]["QUANTITY"] . " " .  $order["ORDER"]["MEASURE_TEXT"]?></div>
				<div class="comment"><?= $order["ORDER"]['USER_DESCRIPTION'] ?></div>			
				</a>
			</div>
			
			<?endforeach?>		
		<?endforeach?>
		</div>
	</div>

		<?if(strlen($arResult['NAV_STRING'])):?>
			<?=$arResult['NAV_STRING']?>
		<?endif?>

	<?else:?>
		<br>
		<br>
		<p style="text-align: center"><?=GetMessage('SPOL_NO_ORDERS')?></p>
	<?endif?>
	

<?endif?>