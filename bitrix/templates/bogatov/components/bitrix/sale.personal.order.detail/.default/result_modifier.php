<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

function ProductProps($arElementIds, $arSelect)
{
	
	if (empty($arElementIds))
		return array();

	$arProductData = array();
	$arElementData = array();
	// obtain list of iblocks we have to deal with
	$res = CIBlockElement::GetList(
		array(),
		array("=ID" => array_unique($arElementIds)),
		false,
		false,
		array("ID", "IBLOCK_ID")
	);
	while ($arElement = $res->GetNext())
		$arElementData[$arElement["IBLOCK_ID"]][] = $arElement["ID"]; // two getlists are used to support 1 and 2 type of iblock properties

	// for each iblock get properties for each element of it
	foreach ($arElementData as $iblockId => $arElemId) // todo: possible performance bottleneck
	{
		$res = CIBlockElement::GetList(
			array(),
			array("IBLOCK_ID" => $iblockId, "=ID" => array_unique($arElemId)),
			false,
			false,
			$arSelect
		);
		while ($arElement = $res->GetNext())
		{
			$id = $arElement["ID"];
			foreach ($arElement as $key => $value)
			{				
				$arProductData[$key] = $value;				
			}
		}
	}

	return $arProductData;
}


$cp = $this->__component;
if (is_object($cp))
{
	CModule::IncludeModule('iblock');

	$hasDiscount = false;
	$hasProps = false;
	$productSum = 0;
	$basketRefs = array();
	$productQuantity = 0;

	$noPict = array(
		'SRC' => $this->GetFolder().'/images/no_photo.png'
	);

	if(is_readable($nPictFile = $_SERVER['DOCUMENT_ROOT'].$noPict['SRC']))
	{
		$noPictSize = getimagesize($nPictFile);
		$noPict['WIDTH'] = $noPictSize[0];
		$noPict['HEIGHT'] = $noPictSize[1];
	}
	
	foreach($arResult["BASKET"] as $k => &$prod)
	{
		if(floatval($prod['DISCOUNT_PRICE']))
			$hasDiscount = true;
		if(!empty($prod['PROPS']))
			$hasProps = true;

		$productSum += $prod['PRICE'] * $prod['QUANTITY'];
		
		$productQuantity++;

		$basketRefs[$prod['PRODUCT_ID']][] =& $arResult["BASKET"][$k];

		if($prod['DETAIL_PICTURE'])
			$prod['PICTURE'] = $prod['DETAIL_PICTURE_THUMB'];
		elseif($prod['PREVIEW_PICTURE'])
			$prod['PICTURE'] = $prod['PREVIEW_PICTURE_THUMB'];
		else
			$prod['PICTURE'] = $noPict;
	}

	$arResult['HAS_DISCOUNT'] = $hasDiscount;
	$arResult['HAS_PROPS'] = $hasProps;

	//$arResult['PRODUCT_SUM_FORMATTED'] = SaleFormatCurrency($productSum, $arResult['CURRENCY']);
	$arResult['PRODUCT_SUM_FORMATTED'] = number_format($productSum, 2, ".", " ");

	if($img = intval($arResult["DELIVERY"]["STORE_LIST"][$arResult['STORE_ID']]['IMAGE_ID']))
	{

		$pict = CFile::ResizeImageGet($img, array(
			'width' => 150,
			'height' => 90
		), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);

		if(strlen($pict['src']))
			$pict = array_change_key_case($pict, CASE_UPPER);

		$arResult["DELIVERY"]["STORE_LIST"][$arResult['STORE_ID']]['IMAGE'] = $pict;
	}
	
	$prod['PROPS'] = ProductProps(array($prod['PRODUCT_ID']), $arParams['CUSTOM_SELECT_PROPS']);
	$arResult["QUANTITY"] = $productQuantity;	
}

$APPLICATION->SetTitle(GetMessage('SPOD_ORDER') . " " . GetMessage("SPOD_NUM_SIGN") . " " . $arResult["ACCOUNT_NUMBER"] ); 
$APPLICATION->AddChainItem($APPLICATION->GetTitle());
?>