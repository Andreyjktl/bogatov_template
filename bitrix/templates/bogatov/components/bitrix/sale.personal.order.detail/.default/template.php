<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(strlen($arResult["ERROR_MESSAGE"])):?>
		<?=ShowError($arResult["ERROR_MESSAGE"]);?>
<? else : ?>
<div class="order-info">
Дата заказа: <span class="date-info"><?= $arResult["DATE_INSERT_FORMATED"] ?></span><br>
<?=GetMessage('SPOD_ORDER_STATUS')?>: <span class="deliv-info"> <?=$arResult["STATUS"]["NAME"]?></span>
</div>
<div class="part-order">
<?=GetMessage('SPOD_ORDER_BASKET')?>
</div>	
<div class="items">
<div class="items-head">
	<div class="head-row">
		<div class="nomenclature">Номенклатура</div>
		<div class="measure">Ед.изм-я</div>
		<div class="order-current">Тек-я заявка (количество)</div>
		<div class="priceval">Цена (Валютная)</div>
		<div class="price">Цена (Рублевая)</div>
		<div class="sumval">Сумма (Валютная)</div>
		<div class="sum">Сумма (Рублевая)</div>
	</div>					
</div>
<div class="items-list">
	<? foreach($arResult["BASKET"] as $prod):?>
	<? //echo "<pre>"; var_dump($prod); echo "</pre>"?>
	<?   $db_props = CIBlockElement::GetProperty("3", $prod["PRODUCT_ID"], "sort", "asc", array());
$PROPS = array();
while($ar_props = $db_props->Fetch())
$PROPS[$ar_props['CODE']] = $ar_props['VALUE']; ?>		

<? // Отображение Props ?>
<? //echo "<pre>"; var_dump($PROPS); echo "</pre>"; ?>
	<div class="item">		
		<div class="nomenclature"><?= $prod["NAME"] ?></div>
		<div class="measure"><?= $prod["MEASURE_TEXT"] ?></div>
		<div class="order-current"><?= number_format($prod["QUANTITY"], 3, ".", "") ?></div>

		<div class="price"><?=$PROPS["PRICEVAL"]?><?= " "?><?=$PROPS["VALUTA"]?></div>

		<div class="price"><?= $prod["PRICE_FORMATED"] ?></div>
		
		<div class="sum"><?= $PROPS["PRICEVAL"] * $prod["QUANTITY"] ?><?= " "?><?=$PROPS["VALUTA"]?></div>
		
		<div class="sum"><?= number_format($prod["PRICE"] * $prod["QUANTITY"], 2, ".", "") ?></div>		

	</div>
	<? endforeach ?>	
</div>
</div>
<div class="total">
	Итоговая сумма: <span><?=$arResult['PRODUCT_SUM_FORMATTED']?></span><img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/ruble-orange.gif" class="ruble-orange-img" />
</div>
<div class="order-detail">	
	<? foreach($arResult["ORDER_PROPS"] as $prop):?>
	<? if( $prop["CODE"] != "FIRM_XML_ID") : ?>
	<div class="some-detail">
		<?= $prop['NAME'] ?>:
		<span><?= str_replace("/",".", $prop["VALUE"]) ?></span>
	</div>
	<? endif ?>
	<? endforeach ?>
	<?if(!empty($arResult["COMMENTS"])):?>
	<div class="some-detail">
		<?=GetMessage('SPOD_ORDER_USER_COMMENT')?>:
		<span><?=$arResult["COMMENTS"]?></span>
	</div>
	<? endif ?>
	<div class="some-detail">
		КОЛИЧЕСТВО ПОЗИЦИЙ:
		<span><?=$arResult['QUANTITY']?> шт.</span>
	</div>
</div>
<? endif ?>