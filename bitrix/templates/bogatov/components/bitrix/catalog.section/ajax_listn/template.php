<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if( $arResult["ITEMS"] ) : ?>
<? foreach($arResult["ITEMS"] as $arElement): ?>
<?// echo "<pre>";  print_r($arElement); echo "</pre>"; ?>		
<div class="numen-item">
	<div class="numen-check-block"><input value="1" name="check[<?= $arElement['ID']?>]" type="checkbox" ></div>
	<div class="article"><?= $arElement["DISPLAY_PROPERTIES"]['ARTICLE']['DISPLAY_VALUE']?></div>
	<div class="nomenclature"><?=$arElement["NAME"]?></div>
	<div class="measure"><?= $arElement['CATALOG_MEASURE_NAME']?></div>
</div>
<? endforeach ?>
<? else: ?>
<p style="text-align: center">Нет доступной номенклатуры для добавления</p>
<? endif ?>
	