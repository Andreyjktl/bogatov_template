BX.ready(function() {
    
    ajax_form_add_bind('.ajax-form-add-address', '.ajax-list-address'); 
    
    $('.ajax-form-add-basket').ajaxForm({
        dataType: 'json',
        success: function(res) {  
            if( res.error === false ) 
            {                 
                $('.ajax-basket-line').html( res.basket_line );              
                $('.fancybox-close').click();
                $('.add-item-to-basket[data-num="' + res.product + '"]').addClass('complete');
                $('.add-item-to-basket[data-num="' + res.product + '"]').removeClass('add-item-to-basket');               
                show_msg_complete(res.msg);
            }
            else
            {              

            }
        }
    });   


    $('.add-item-to-basket').click( function(){
        var $num = $(this).attr('data-num');
        var $article = $('.item-' + $num).find('.article').text();
        var $nomenclature = $('.item-' + $num).find('.nomenclature').text();
        var $measure = $('.item-' + $num).find('.measure').text();
        var $max_qnt = $(this).attr('data-max-qnt');
       
        $('#product_order').val($num);
        $('#ajax_article_order').text($article);
        $('#ajax_nomenclature_order').text($nomenclature);
        $('#order-measure').text($measure);
        $('.number-input').val('1');
        $('.number-input').attr('data-max-qnt',$max_qnt);         
    })   
});
