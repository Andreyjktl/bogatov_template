<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="items">
	<div class="items-head">
		<div class="head-row">
			<div class="article">Артикул</div>
			<div class="nomenclature_l">Номенклатура</div>
			<div class="measure">Ед.изм-я</div>
			<div class="balance">Доступный остаток</div>
			<div class="reserve">Заявки принятые (резерв) </div>
			<div class="road">Товар в пути (кол-во в дост-е)</div>
			<div class="items-basket"></div>
		</div>
	</div>
	<div class="items-list">
		<? if($arResult["ITEMS"]) : ?>
		<? foreach($arResult["ITEMS"] as $arElement): ?>
		<? //echo "<pre>";  print_r($arElement); echo "</pre>"; ?>
		<div class="item item-<?= $arElement['ID']?>">
			<div class="article"><?= $arElement["DISPLAY_PROPERTIES"]['ARTICLE']['DISPLAY_VALUE']?></div>
			<div class="nomenclature_l"><?=$arElement["NAME"]?></div>
			<div class="measure"><?= $arElement['CATALOG_MEASURE_NAME']?></div>
			
			<div class="balance"><?= number_format($arElement['QUANTITY'],3,".","") ?></div>
			
			<div class="reserve"><?= $arElement["DISPLAY_PROPERTIES"]['RESERVED']['DISPLAY_VALUE'] ? number_format($arElement["DISPLAY_PROPERTIES"]['RESERVED']['DISPLAY_VALUE'], 3,".","") : number_format(0, 3,".","") ?></div>
			<div class="road"><?= number_format($arElement["DISPLAY_PROPERTIES"]['IN_DELIVERY']['DISPLAY_VALUE'],3,".","") ?></div>	
			<div class="items-basket"><a data-num="<?= $arElement['ID']?>" href="#order" data-max-qnt="<?= $arElement['QUANTITY']?>" class="add-item-to-basket <?= $arElement['COMPLETE'] ? "complete" : "" ?>"></a></div>			
		</div>	
		<? endforeach ?>
		<? else: ?>
		<p style="text-align: center">Нет доступной номенклатуры</p>
		<? endif?>
	</div>
</div>
<? if (strlen($arResult["NAV_STRING"]) > 0):?><?= $arResult["NAV_STRING"] ?><?endif?>
<div style="display:none">	
	<div class="popup-order" id="order">
		<form action="/ajax/basket.php" method="post" class="ajax-form-add-basket">
			<input type="hidden" value="add_to_basket" name="action">
			<input id="product_order" type="hidden" name="product" value="">
			<div class="order-article">Артикул<span id="ajax_article_order">025489</span></div>
			<div id="ajax_nomenclature_order" class="order-nomenclature">Пузыри 20х20</div>
			<span class="number">Количество, <span id="order-measure"></span></span>			
			<input name="number" data-max-qnt="1" value="1" type="text"  class="number-input" placeholder="1"/>
			<button class="order-button" type="submit">Заказать</button>
		</form>
	</div>	
</div>