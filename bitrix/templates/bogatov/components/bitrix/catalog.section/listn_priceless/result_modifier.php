<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//Make all properties present in order
//to prevent html table corruption
CModule::IncludeModule("sale");
		
foreach($arResult["ITEMS"] as $key => $arElement)
{
	//echo "<pre>"; var_dump($arElement); echo "</pre>";
	$arRes = array();
	foreach($arParams["PROPERTY_CODE"] as $pid)
	{
		$arRes[$pid] = CIBlockFormatProperties::GetDisplayValue($arElement, $arElement["PROPERTIES"][$pid], "catalog_out");
	}
	$arResult["ITEMS"][$key]["DISPLAY_PROPERTIES"] = $arRes;
	
	$arCatalogProduct = CCatalogProduct::GetByID($arElement['ID']);	
	
	$arResult["ITEMS"][$key]['QUANTITY'] = $arCatalogProduct['QUANTITY'];
	//$arResult["ITEMS"][$key]['QUANTITY_RESERVED'] = $arCatalogProduct['QUANTITY_RESERVED'];

	//Проверка, наличие товара в корзине
	$arResult["ITEMS"][$key]['COMPLETE'] = false;
	
	$dbBasketItems = CSaleBasket::GetList(
		array(
				"NAME" => "ASC",
				"ID" => "ASC"
			),
		array(
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL",
				"PRODUCT_ID" => $arElement['ID']
			),
		false,
		false,
		array("ID")
    );
	
	while ($arItems = $dbBasketItems->Fetch())
	{	
		$arResult["ITEMS"][$key]['COMPLETE'] = true;
	}
		
	//Подсчет кол-во товара в доставке
	/*$arOBasket = array();
	$dbBasket = CSaleBasket::GetList(array('NAME' => 'asc'), 
			array(
				"PRODUCT_ID"=>$arElement['ID'],
				"ORDER_STATUS" => "D"
			), false, false, array('*'));
	while ($arBasket = $dbBasket->Fetch())
	{
		if (CSaleBasketHelper::isSetItem($arBasket))
			continue;

		$arOBasket[] = $arBasket;
	}	
	
	$arResult["ITEMS"][$key]['IS_DELIVERY'] = count($arOBasket);
	*/
}
?>