<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<p class="currency-list">
<?if ($arParams["SHOW_CB"] == "Y"):?>
	<tr>
		<td colspan="3"><b><?=GetMessage("CURRENCY_SITE")?></b></td>
	</tr>
<?endif;?>

<?foreach ($arResult["CURRENCY"] as $key => $arCurrency):?>
	<?=$arCurrency["FROM"]?> = <?= number_format($arCurrency["BASE"], 2, ".", " "); ?>
<?endforeach?>

<?if (is_array($arResult["CURRENCY_CBRF"]) && $arParams["SHOW_CB"] == "Y"):?>
	<?=GetMessage("CURRENCY_CBRF")?>
	<?foreach ($arResult["CURRENCY_CBRF"] as $arCurrency):?>
		<?=$arCurrency["FROM"]?> = <?= number_format($arCurrency["BASE"], 2, ".", " "); ?>
	<?endforeach?>
<?endif?>
