<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<p class="currency-list">
Курсы валют:
<?foreach ($arResult["CURRENCY"] as $key => $arCurrency):?>
	
		<?=$arCurrency["FROM"]?> = <?= number_format($arCurrency["BASE"], 2, ".", " "); ?>  
	
<?endforeach?>

</p>