<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<ul class="sections">
	<? foreach($arResult as $arItem):?>
	<li><a href="<?=$arItem["LINK"]?>" class="<?= $arItem["SELECTED"] ? "current" : "" ?>" ><?=$arItem["TEXT"]?></a></li>		
	<? endforeach ?>
</ul>
<?endif?>