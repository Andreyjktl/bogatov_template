<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!doctype html>
<html>
<head>
<title><?$APPLICATION->ShowTitle()?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php		
	$APPLICATION->ShowMeta("robots", false, true);
	$APPLICATION->ShowMeta("keywords", false, true);
	$APPLICATION->ShowMeta("description", false, true);	
?>	
<?php
  header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s")." GMT");
  header("Cache-Control: no-cache, must-revalidate");
  header("Cache-Control: post-check=0,pre-check=0", false);
  header("Cache-Control: max-age=3600", false);
  header("Pragma: no-cache");
?>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>	
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/reset.css")?>" />
	
	
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/style.css")?>" />
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/bootstrap.css")?>" />
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/jquery.fancybox.css")?>" />
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/jquery-ui-1.10.4.custom.css")?>" />
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/jquery.formstyler.css")?>" />
	
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery-ui-1.10.4.custom.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.formstyler.min.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.mousewheel.min.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.mask.min.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.fancybox.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.jStepper.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/action.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.form.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/pactions.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.confirm.min.js"></script>
	<? $APPLICATION->ShowHead(); ?>
	<? $APPLICATION->ShowCSS(true, true); ?>
</head>
<body>
	<div class="wrap">
	<?= $APPLICATION->ShowPanel()?>
	<div class="lk-header">
		<div class="wrapper">
			<a href="/" class="lk-logo"><img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/lk-logo.png"></a>
			<div class="lk-adress">
				<? $APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/include/address.php"
					)
				);?>
			</div>
			<div class="lk-phone">
				<? $APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/include/phone.php"
					)
				);?>
			</div>
			<? if(!$USER->IsAdmin()) : ?>
			<span class="ajax-basket-line">
			<?
				$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", ".default", array(
				"PATH_TO_BASKET" => SITE_DIR."profile/basket/",
				"PATH_TO_PERSONAL" => SITE_DIR."profile/basket/",
				"SHOW_PERSONAL_LINK" => "N"
				),
				false,
				Array('')
				);
			?>
			</span>
			<? endif ?>
			<div class="user-name">				
				<?= $USER->GetFullName(); ?>
			</div>
			<a  href="/?logout=yes" class="exit-login">Выход</a>
		</div>
	</div>
	<div class="lk-main">
		<div class="wrapper">			
			<?	$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "podmenu",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y"
	),
	false
);?>
			<div class="broadcamp admin">			
				<?$APPLICATION->IncludeComponent(
					"bitrix:breadcrumb",
					"",
					Array(
						"START_FROM" => "0",
						"PATH" => "",
						"SITE_ID" => "s1"
					)
				);?>
			</div>
			<div class="lk-head">
				<span><?= $APPLICATION->ShowTitle() ?></span>
			</div>