$(document).ready(function() {
/*		ввод в инпут number только чисел 	*/
function onlyNum(e){
	e.bind("change keyup input click", function() {
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
	});
}

onlyNum($('.order-current input'));

/*		фиксированный футер у таблицы 	*/
if($('.inditer').length){
var topPos = $('.inditer').offset().top;
$(window).scroll(function(){
	//console.log($(".statistic-total").offset().top);
	//console.log($(".items-list").offset().top);
		var top = $(document).scrollTop() + $(window).height() -  $(".statistic-total").height();
		if  (top < topPos){
		
		   $(".statistic-total").css({
					position: 'fixed',
					bottom: 0					
				});
		}
		if  (top  >= topPos ){
		   $(".statistic-total").css("position", "relative");
		}
	});
}


/*		стилизация формы		*/
			$('input').styler();
			$('select').styler();

/*		admin-icon		*/
$('.admin-icon').click(function () {

	$('.admin-icon ul').each(function(){
		if($(this).is(':visible')){
			$(this).hide(30);
		}
	});
    var myBlock = $(this).find('ul');
      if (myBlock.css('display') != 'block') {
        /*		умное поз	*/

										var win = $(window);
										var topOffset = myBlock.parent().offset().top;
										var bottomOffset = win.height() -  (topOffset - win.scrollTop());
										var liHeight = 37;
										var	minHeight = liHeight * 2;
										var	newHeight = 'auto';
                                        var position = myBlock.css('top');

										// раскрытие вниз
										if (bottomOffset > (minHeight) + 20)	{
											myBlock.height('auto').css({bottom: 'auto', top:position });
                                            myBlock.removeClass('bottom');
                                            myBlock.addClass('top');
											function maxHeightBottom() {
												myBlock.css('max-height', Math.floor((bottomOffset) / liHeight) * liHeight);
											}
											maxHeightBottom();
											myBlock.css('max-height', newHeight);

											if (bottomOffset < (myBlock.outerHeight())) {
												maxHeightBottom();
											}

										// раскрытие вверх
										} else {
											myBlock.height('auto').css({top: 'auto', bottom: position});
                                            myBlock.removeClass('top');
                                            myBlock.addClass('bottom');
											function maxHeightTop() {
												myBlock.css('max-height', Math.floor((topOffset - win.scrollTop()) / liHeight) * liHeight);
											}
											maxHeightTop();
											myBlock.css('max-height', newHeight);

											if ((topOffset - win.scrollTop()) < (myBlock.outerHeight())) {
												maxHeightTop();
											}
										}


		myBlock.show(30);

		}
		var yourClick = true;
        $(document).bind('click.myEvent', function (e) {
          if (!yourClick && $(e.target).closest(myBlock).length == 0) {
            myBlock.hide(30);
            $(document).unbind('click.myEvent');
          }
          yourClick = false;
        });
});


/*		всплывашка заказа		*/
			$('.items-basket a').fancybox({
				openEffect	: 'fade',
				closeEffect	: 'fade',
				prevEffect		: 'none',
				nextEffect		: 'none',
				centerOnScroll: 'false',
				scrolling:'no',
				fitToView  : false,
				scrollOutside: true,
				padding:0,
				top:50,
				tpl: {
					closeBtn : '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
				},
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.60)'
						}
					}

				}
			});
/*		всплывашка добавить организацию		*/
			$('.add-org').fancybox({
				openEffect	: 'fade',
				closeEffect	: 'fade',
				prevEffect		: 'none',
				nextEffect		: 'none',
				centerOnScroll: 'false',
				scrolling:'no',
				fitToView  : false,
				scrollOutside: true,
				padding:0,
				top:50,
				tpl: {
					closeBtn : '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
				},
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.60)'
						}
					}

				}
			});
			$('.org-cancel').click(function(){
				$.fancybox.close( true )
			});

/*		всплывашка добавить пользователя		*/
			$('.add-prof').fancybox({
				openEffect	: 'fade',
				closeEffect	: 'fade',
				prevEffect		: 'none',
				nextEffect		: 'none',
				centerOnScroll: 'false',
				scrolling:'no',
				fitToView  : false,
				scrollOutside: true,
				padding:0,
				top:50,
				tpl: {
					closeBtn : '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
				},
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.60)'
						}
					}

				}
			});


/*			редактирование справочной информации		*/

	//переменная для сохранения значения из ячейки редактирования
	var epicData=[];//переменная для сохранения удалённых строк

		// функция checked

$.fn.toggleChecked = function() {
		return this.each(function() {
			this.checked = !this.checked;
		});
	};

	//	 для добавления и удаления инпутов
	var epicVal= [];
	var name;
	var id;
	var data;
	var counter = 0;
function inputElements(element, e, action){
	var checkName;
	var sub;
	var mass = ['.needs', '.stock', '.FIO', '.post', '.login'];
	var x ;
	var k ;
	element.each(function(){		
		for(var i = 0; i  < mass.length ; i++){	
			x = $(this).find(mass[i]);
			checkName = $(this).find('input:checkbox').attr('value');		
			id = $(this).attr('id');
			if(e == 0){
					//console.log(x.text()+'\n');
				if(x.find('input').length){
					return false;
				}else{	
					name = id + '/' + x.attr('class');
					data = x.text();
					epicVal[name] = data;								
					x.text('');
					x.append("<input class='edit-num' type='text'>");
					x.find('.edit-num').attr({'name' : checkName}).val(epicVal[name]);
					if(x.parents('#as2')){ //типо admin
						if($(this).find('.adm-check').length > 0){return false;}					
						$(this).find('.agree').append("<input type='checkbox' class='adm-check'>");
						$('input').styler();
					}
				}
			}else{	
				if(action == 0){ //для cancel
					name = id + '/' + x.attr('class');					
					x.find('.edit-num').remove();
					x.text(epicVal[name]);	
					
				}else{	 //для save'a				
					sub = x.find('.edit-num').val();
					x.find('.edit-num').remove('.edit-num');				
					x.text(sub);
				}
			}
	}
	});	
}
	//для добавления и удаления чекбоксов в access
function accessCheck(elem, action, absolutely){
	if(elem.find('.access').length > 0){
		if(action == 1){
			var checkIt = elem.find('.access').find('img');
			var checkDiv = elem.find('.access').find('input'); 
			if(checkIt.length > 0 || checkDiv.length > 0){
				if(checkDiv.length > 0 ){
					if(checkDiv.is(':checked')){
						//console.log('чекнут чекбокс, вставляем чекбокс с галочкой (elem, 1)');
						elem.find('.access').html('');
						elem.find('.access').append("<input type='checkbox' checked='checked'>").prop('checked', true); $('input').styler();
					}else{
						//console.log(' чекбокс не чекнут, вставляем чекбокс без (elem, 1)');
						elem.find('.access').html('');
						elem.find('.access').append("<input type='checkbox' >").prop('checked', true); $('input').styler();
					}				
				}else{
					//console.log('галочка есть, вставляем чекбокс с галочкой (elem, 1)');
					elem.find('.access').find('img').remove();
					elem.find('.access').append("<input type='checkbox' checked='checked'>").prop('checked', true); $('input').styler();
				}
			}else{	
				//console.log('галочки нет, вставляем чекбокс (elem, 1)');			
				elem.find('.access').append("<input type='checkbox'>");  $('input').styler();
			}
		}else{
			if(absolutely == 1){
				//console.log('не важно, удаляем всё (elem, 0 , 1)');
				elem.find('.access').html('');
			}else{			
				if(elem.find('.access input').is(':checked')){
					//console.log('галочка есть, удаляем чекбокс, вставляем галочку (elem, 0)');
					elem.find('.access').html('');
					elem.find('.access').append("<img src='assets/images/checked.gif'>");
				}else{
					//console.log('галочки нет, удаляем всё (elem, 0)');
					elem.find('.access').html('');
				}
			}
		}
	}
}

	//упрощения
    var o;
    o = {
        group_e_b: $('.group-edit-button'),
        group_d_b: $('.group-delete-button'),
        save_b: $('.save-button'),
        cancel_b: $('.cancel-button'),
        add_b: $('.add-button'),
        admin_i_l : $('.admin .items-list .item')
    };

    //	кнопка редактирования группы
    o.group_e_b.click(function(){
		$('.admin .editItem').each(function(){
			accessCheck($(this), 1)
		    inputElements($(this), 0, 0);
            o.group_e_b.hide(100);
            o.group_d_b.hide(100);
            o.save_b.show(100);
            o.cancel_b.show(100);
		});
});
	//	кнопка удаления группы
    o.group_d_b.click(function(){
		var dataNum = 0;
		$('.admin .editItem').each(function(){
			epicData[dataNum] = $(this).detach();
			dataNum++;
		});
        o.group_e_b.hide(100);
        o.group_d_b.hide(100);
        o.save_b.show(100);
        o.cancel_b.show(100);
});

	//	главный чекбокс
$('.admin .items-head input').change(function(){
	if($(this).is(':checked')){
		var subject = $('.admin .item');
		inputElements(subject, 1 );
		o.admin_i_l.each(function(){
			if($(this).hasClass('edited')){

			}else {
				$(this).find('.delete').css('display', 'none');
				$(this).addClass('editItem');				
				$(this).find('.check-block input').toggleChecked().trigger('refresh');
				$(this).find('.edit').addClass('edit-group');				
                o.add_b.hide(100);
                o.save_b.hide(100);
                o.cancel_b.hide(100);
                o.group_e_b.show(100);
                o.group_d_b.show(100);
				$('.statistic-total').css('background', '#b7b7b7');
			}
		});

	}else{
		var myNum = epicData.length;
		for(var i=0; i <= myNum; i++){
			$('.admin .items-list').append(epicData[i]);
		}
		o.admin_i_l.each(function(){
			if($(this).hasClass('edited')){

                }else {
                $(this).removeClass('editItem');
				$(this).find('.edit').removeClass('active');
                $(this).find('.edit').removeClass('edit-group');
                $(this).find('.check-block input').prop('checked', false).trigger('refresh');
                $(this).find('.delete').css('display', 'none');				
                inputElements($(this), 1, 0);
				accessCheck($(this), 0, 1);
                o.save_b.hide(100);
                o.cancel_b.hide(100);
                o.group_e_b.hide(100);
                o.group_d_b.hide(100);
                $('.statistic-total').css('background', 'white');
                o.add_b.show(100);
			}
		dataNum = 0;
		myNum = 0;
		});
	}
});

	//	функция построчного редактирования
	//	для групового редактирования
function groupEdit(yours){
		var subject = $('.admin .editItem');
			inputElements(subject, 1, 0);
			accessCheck(subject, 0, 1)
		if(yours.parents('.item').hasClass("edited")){
		}else{
				o.admin_i_l.find('.edit').removeClass('active').addClass('edit-group');
				o.admin_i_l.find('.delete').css('display', 'none');				
				var item = yours.parents('.item');				
				item.addClass('editItem');
				$('.admin .items-head').find('.check-block input').prop('checked', true).trigger('refresh');
				o.add_b.hide(100);
				o.save_b.hide(100);
				o.cancel_b.hide(100);
				o.group_e_b.show(100);
				o.group_d_b.show(100);
				$('.statistic-total').css('background', '#b7b7b7');
		}
}
	//	для клика
function postrochnoeClick(yours){
	yours.click(function(){
		if($(this).parents('.item').hasClass("edited")){
		}else{
			var item = $(this).parents('.item');
			var max = $('.item .check-block input:checked').length;
			if( max > 0 ){
				return false;
			}else {
				item.addClass('editItem');
				item.find('.edit').addClass('active');
				item.find('.check-block input').prop('checked', true).trigger('refresh');
				$('.admin .items-head').find('.check-block input').prop('checked', true).trigger('refresh');
				item.find('.delete').css('display', 'inline-block');
				inputElements(item, 0);
				accessCheck(item, 1)				
				o.add_b.hide(100);
				o.save_b.show(100);
				o.cancel_b.show(100);
				$('.statistic-total').css('background', '#b7b7b7');
			}
		}
	});

}
	//	для чекбокса
function postrochnoeCheck(yours){
	yours.change(function(){
			var item = $(this).parents('.item');
			var max = $('.admin .items-list .check-block input:checked').length;
			if(!item.hasClass('editItem')){
				$('.admin .items-head').find('.check-block input').prop('checked', true).trigger('refresh');
				if( max > 1){
					groupEdit($(this));
				}else{
					item.addClass('editItem');
					item.find('.edit').addClass('active');
					item.find('.delete').css('display', 'inline-block');
					inputElements(item, 0);						
					accessCheck(item, 1)
					o.add_b.hide(100);
					o.save_b.show(100);
					o.cancel_b.show(100);
					o.group_e_b.hide(100);
					o.group_d_b.hide(100);
					$('.statistic-total').css('background', '#b7b7b7');
				}
			}else{
				if(max == 1){						
					item.removeClass('editItem');
					inputElements(item, 1);	
					accessCheck(item, 0, 1);					
					var miniItem = $('.editItem');
					miniItem.find('.edit').addClass('active');
					miniItem.find('.delete').css('display', 'inline-block');
					inputElements(miniItem, 0);					
					accessCheck(miniItem, 1);
					
					o.admin_i_l.find('.edit').removeClass('edit-group');
					o.add_b.hide(100);
					o.save_b.show(100);
					o.cancel_b.show(100);
					o.group_e_b.hide(100);
					o.group_d_b.hide(100);
					$('.statistic-total').css('background', '#b7b7b7');
				}else {								
					item.removeClass('editItem');
					item.find('.edit').removeClass('active');
					item.find('.delete').css('display', 'none');
					inputElements(item, 1);								
					if(max == 0){
						$('.admin .items-head').find('.check-block input').prop('checked', false).trigger('refresh');
						accessCheck(item, 0, 1)
						o.add_b.show(100);
						o.save_b.hide(100);
						o.cancel_b.hide(100);
						$('.statistic-total').css('background', 'white');
					}
					if(!item.find('input')){
						o.save_b.hide(100);
						o.cancel_b.hide(100);
					}
				}

			}
	});
}

postrochnoeClick($('.admin .edit-wrap>.edit'));
postrochnoeCheck($('.admin .items-list .check-block input'));

	//	кнопка delete
	var dataNum = 0;
	$('.delete').click(function(){
		var item = $(this).parent().parent();
		epicData[dataNum] = item.detach();
		dataNum++;
	});
	//	кнопка del-item
	$('.del-item a').click(function(){
		var item = $(this).parent().parent();
		item.remove();

	});

	//	кнопка cancel
	o.cancel_b.click(function(){
		var myNum = epicData.length;
		for(var i=0; i <= myNum; i++){
			$('.admin .items-list').append(epicData[i]);
		}
		$('.admin .item').each(function(){
			$(this).removeClass('editItem');
			$(this).find('.edit').removeClass('active').removeClass('edit-group');
			if($(this).find('.check-block input').is(':checked')){
				$(this).find('.check-block input').prop('checked', false).trigger('refresh');
			}
			$('.admin .items-head').find('.check-block input').prop('checked', false).trigger('refresh');
			$(this).find('.delete').css('display', 'none');
			$(this).find('.adm-check').remove();
			accessCheck($(this), 0, 1)
			inputElements($(this), 1, 0);
			
			o.save_b.hide(100);
			o.cancel_b.hide(100);
			$('.statistic-total').css('background', 'white');
			o.add_b.show(100);
		});
		dataNum = 0;
		myNum = 0;
	});

/*		Календарь	*/
$( "#shipping" ).datepicker({
	  dateFormat: 'dd/mm/yy',
      showOn: "button",
      buttonImage: "assets/images/calendar-icon.png",
      buttonImageOnly: true
    });
if($("#shipping").length){
	$("#shipping").mask('99/99/9999');
}

});//document ready

