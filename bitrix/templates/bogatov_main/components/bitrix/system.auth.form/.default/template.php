<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="login-menu">
	<span>Вход в личный кабинет</span>
	<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
		<div class="formDiv">
			<label for="login">Логин</label>
			<input type="text" id="login" name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>" placeholder="">
		</div>
		<div class="formDiv">
			<label for="pass">Пароль</label>
			<input type="password" id="pass" name="USER_PASSWORD" placeholder="">
		</div>
		<div class="formDiv">						
			<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y"  class="checkbox"><div class="check-span">Запомнить меня</div>
		</div>  
		<div class="formDiv">						
			<button name="logout_butt" class="sub-button" type="submit">Войти</button>	
			<a href="#" class="forgotten">Забыли свой пароль?</a>
		</div> 
		<? if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) : ?>
		<div class="form-error">Неверный логин или пароль</div>		
		<? else: ?>		
		<? ;//LocalRedirect('/doc/index.php?docs=2054') ?>
		<? endif ?>
		<div class="form-support"><?= $arParams["SUPPORT_MESS"]?></div>
	</form>
</div>