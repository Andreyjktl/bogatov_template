<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!doctype html>
<html>
<head>
<title><?$APPLICATION->ShowTitle()?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php		
	$APPLICATION->ShowMeta("robots", false, true);
	$APPLICATION->ShowMeta("keywords", false, true);
	$APPLICATION->ShowMeta("description", false, true);	
?>	
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>	
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/reset.css")?>" />
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/style.css")?>" />
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/jquery.fancybox.css")?>" />
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/jquery-ui-1.10.4.custom.css")?>" />
	<link rel="stylesheet" type="text/css" href="<?=CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH."/assets/css/jquery.formstyler.css")?>" />
	
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery-1.10.2.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery-ui-1.10.4.custom.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.formstyler.min.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.fancybox.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/action.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/jquery.form.js"></script>
	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/pactions.js"></script>
	<? $APPLICATION->ShowHead(); ?>
	<? $APPLICATION->ShowCSS(true, true); ?>
</head>
<body>
<div class="wrap">
	<div class="header">
		<div class="wrapper">
			<div class="adress">
				<? $APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/include/address.php"
					)
				);?></div>
			<div class="phone">
				<? $APPLICATION->IncludeComponent(
					"bitrix:main.include",
					"",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "inc",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/include/phone.php"
					)
				);?></div>
		</div>
	</div>
	<div class="login-page">
		<div class="logo-main"></div>
		<div class="wrapper">
			<a href="#" class="main-logo"><img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/logo.png"></a>