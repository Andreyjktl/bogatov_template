
$(document).ready(function() {

   //всплывалка забыли пароль
   $('.forgotten').click(function (e) {
      var cur;
      var $myBlock = $('.form-support'); 	  
      if ($myBlock.css('display') != 'block') {
        $myBlock.show();
        var yourClick = true;
        $(document).bind('click.myEvent', function (e) {
          if (!yourClick && $(e.target).closest('.form-support').length == 0) {
            $myBlock.hide();
            $(document).unbind('click.myEvent');
          }
          yourClick = false;
        });
      }
      $('.form-error').toggle();
      e.preventDefault();
    });
	
    $('.edited .agree').text(' ');

	//только числа 
	if($(".edit-num").length) {
	$('.edit-num').keypress(function(e) {
					if (!(e.which==8 || e.which==46   ||(e.which>47 && e.which<58))) return false;
				});
	}
  
    $('.edited .agree').text(' ');

	//только числа 
	if($(".edit-num").length) {
	$('.edit-num').keypress(function(e) {
					if (!(e.which==8 || e.which==46   ||(e.which>47 && e.which<58))) return false;
				});
	}
	//числа при добавлении в карзину
	if($(".number-input").length) {
		$('.number-input').jStepper({minValue:0, maxValue:23, allowDecimals:false });	
	}

	//маска календаря
	$('.shipping').mask('99/99/9999');
	
	//валидация корзины
	if($("#valid").length){
		$("#valid").validate(); 
		$("#valid").removeAttr("novalidate");
		$.validator.messages.required = "Заполните поле.";
	}
	if($("#new-user").length){
		$("#new-user").validate(); 
		$("#new-user").removeAttr("novalidate");
		$.validator.messages.required = "Заполните поле.";
	}
	
		
	
    /*		всплывашка заказа		*/
    $('.items-basket a').not('.complete').fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
        prevEffect: 'none',
        nextEffect: 'none',
        centerOnScroll: 'false',
        scrolling: 'no',
        fitToView: false,
        scrollOutside: true,
        padding: 0,
        top: 50,
        tpl: {
            closeBtn: '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
        },
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.60)'
                }
            }

        }
    });
	
	 $('.add-button').fancybox({
		
        openEffect: 'fade',
        closeEffect: 'fade',
        prevEffect: 'none',
        nextEffect: 'none',
        centerOnScroll: 'false',
        scrolling: 'no',
        fitToView: false,
        scrollOutside: true,
        padding: 0,
        top: 50,
        tpl: {
            closeBtn: '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
        },
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.60)'
                }
            }

        }
    });
	
    /*		всплывашка добавить организацию		*/
    $('.add-org').fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
        prevEffect: 'none',
        nextEffect: 'none',
        centerOnScroll: 'false',
        scrolling: 'no',
        fitToView: false,
        scrollOutside: true,
        padding: 0,
        top: 50,
        tpl: {
            closeBtn: '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
        },
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.60)'
                }
            }

        }
    });

    $('.org-cancel').click(function() {
        $.fancybox.close(true)
    });

    //	дефолтные фенси	
    $.extend($.fancybox.defaults, {
        padding: 0,
        fitToView: true,
        padding: 0,
                fixed: false,
        autoCenter: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.60)'
                }
            }
        }
    });

    /*		всплывашка добавить пользователя		*/
    $('.add-prof').fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
        prevEffect: 'none',
        nextEffect: 'none',
        centerOnScroll: 'false',
        scrolling: 'no',
        fitToView: false,
        fixed: false,
        scrollOutside: true,
        padding: 0,
        top: 50,
        tpl: {
            closeBtn: '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
        },
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.60)'
                }
            }

        }
    });

    /*		всплывашка сменить пароль*/
    $('.changePass').fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
        prevEffect: 'none',
        nextEffect: 'none',
        centerOnScroll: 'false',
        scrolling: 'no',
        fitToView: false,
        fixed: false,
        scrollOutside: true,
        padding: 0,
        top: 50,
        tpl: {
            closeBtn: '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
        },
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.60)'
                }
            }

        }
    });

    /*		всплывашка добавить адрес*/
    $('.add-deliv-adres').fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
        prevEffect: 'none',
        nextEffect: 'none',
        centerOnScroll: 'false',
        scrolling: 'no',
        fitToView: false,
        fixed: false,
        scrollOutside: true,
        padding: 0,
        top: 50,
        tpl: {
            closeBtn: '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
        },
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.60)'
                }
            }

        }
    });

    /*		Календарь	*/
   

    startMagic();

});//document ready
/*		всплывашки 	*/

// форма ОТРИЦАТЕЛЬНАЯ
function errorMsg(text) {
    $.fancybox("<div class='popup-er' id='error-window'><div class='msg'>" + text + "</div></div>",
            {
                openEffect: 'fade',
                closeEffect: 'fade',
                prevEffect: 'none',
                nextEffect: 'none',
                centerOnScroll: 'false',
                scrolling: 'no',
                fitToView: false,
                fixed: false,
                scrollOutside: true,
                padding: 0,
                tpl: {
                    closeBtn: '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
                },
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 0, 0, 0.60)'
                        }
                    }
                },
				afterLoad: function(){
				   setTimeout( function() {$.fancybox.close(); },1000); 
				  }
            });
}

// форма ПОЛОЖИТЕЛЬНАЯ
function goodMsg(text) {
    $.fancybox("<div class='popup-good' id='good-window'><div class='msg'>" + text + "</div></div>",
            {
                openEffect: 'fade',
                closeEffect: 'fade',
                prevEffect: 'none',
                nextEffect: 'none',
                centerOnScroll: 'false',
                scrolling: 'no',
                fitToView: false,
                fixed: false,
                scrollOutside: true,
                padding: 0,
                tpl: {
                    closeBtn: '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
                },
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 0, 0, 0.60)'
                        }
                    }
                },
				afterLoad: function(){
				   setTimeout( function() {$.fancybox.close(); },1000); 
				  }
            });
}

// форма с ДА и ОТМЕНА
function confirmMsg(text) {
    $.fancybox("<div class='popup-confirm' ><div class='confirm'><div class='msg2'>" + text + "</div><div>	<button class='dell' type=submit>Да</button><button class='canc' type='reset'>Отменить</button></div></div></div>",
            {
                openEffect: 'fade',
                closeEffect: 'fade',
                prevEffect: 'none',
                nextEffect: 'none',
                closeClick: true,
                centerOnScroll: 'false',
                scrolling: 'no',
                fitToView: false,
                fixed: false,
                scrollOutside: true,
                padding: 0,
                top: 50,
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 0, 0, 0.60)'
                        }
                    }

                }

            });
}
//$('.lk-head span').click(function(){confirmMsg('Удалить форму?')}); пример

/*		фиксированный футер у таблицы 	*/
function initEditmenu() {
    if ($('.inditer').length) {
        var topPos = $('.inditer').offset().top;
        $(window).scroll(function() {
            // //console.log($(".statistic-total").offset().top);
            // //console.log($(".items-list").offset().top);
            var top = $(document).scrollTop() + $(window).height() - $(".statistic-total").height();
            if ($('input').is(':checked')) {
                if (top < topPos) {
                    $(".statistic-total").css({
                        position: 'fixed',
                        bottom: 0
                    });
                    $("form").css({
                        marginBottom: '100px'
                    });
                }
            }

            if (top >= topPos) {
                $(".statistic-total").css("position", "relative");
                $("form").css({
                    marginBottom: '0'
                });
            }
        });
        var top = $(document).scrollTop() + $(window).height() - $(".statistic-total").height();
        //console.log(top, topPos)
        if ($('input').is(':checked')) {
            if (top < topPos) {
                $(".statistic-total").css({
                    position: 'fixed',
                    bottom: 0
                });
            }
        } else {

            $(".statistic-total").css("position", "relative");

        }

    }
}

function startMagic() {
		
    //	упрощения
    var o;
    o = {
        group_e_b: $('.group-edit-button'),
        group_d_b: $('.group-delete-button'),
        save_b: $('.save-button'),
        cancel_b: $('.cancel-button'),
        add_b: $('.add-button'),
        admin_i_l: $('.items-list .item'),
        dell_b: $('.del-item a')
    };

    /*		стилизация формы		*/
    $('input').styler();
    $('select').styler();

    /*		admin-icon		*/
    $('.admin-icon').click(function() {
        $('.admin-icon ul').each(function() {
            if ($(this).is(':visible')) {
                $(this).hide();
            }
        });
        var myBlock = $(this).find('ul');
        if (myBlock.css('display') != 'block') {
            /*		умное поз	*/

            var win = $(window);
            var topOffset = myBlock.parent().offset().top;
            var bottomOffset = win.height() - (topOffset - win.scrollTop());
            var liHeight = 37;
            var minHeight = liHeight * 2;
            var newHeight = 'auto';
            var position = myBlock.css('top');

            // раскрытие вниз
            if (bottomOffset > (minHeight) + 20) {
                myBlock.height('auto').css({bottom: 'auto', top: position});
                myBlock.removeClass('bottom');
                myBlock.addClass('top');
                function maxHeightBottom() {
                    myBlock.css('max-height', Math.floor((bottomOffset) / liHeight) * liHeight);
                }

                maxHeightBottom();
                myBlock.css('max-height', newHeight);

                if (bottomOffset < (myBlock.outerHeight())) {
                    maxHeightBottom();
                }

                // раскрытие вверх
            } else {
                myBlock.height('auto').css({top: 'auto', bottom: position});
                myBlock.removeClass('top');
                myBlock.addClass('bottom');
                function maxHeightTop() {
                    myBlock.css('max-height', Math.floor((topOffset - win.scrollTop()) / liHeight) * liHeight);
                }

                maxHeightTop();
                myBlock.css('max-height', newHeight);

                if ((topOffset - win.scrollTop()) < (myBlock.outerHeight())) {
                    maxHeightTop();
                }
            }


            myBlock.show();

        }
        var yourClick = true;
        $(document).bind('click.myEvent', function(e) {
            if (!yourClick && $(e.target).closest(myBlock).length == 0) {
                myBlock.hide();
                $(document).unbind('click.myEvent');
            }
            yourClick = false;
        });
    });


    /*			редактирование справочной информации		*/

    //переменная для сохранения значения из ячейки редактирования
    //var epicData=[];//переменная для сохранения удалённых строк
    var mass = ['.needs', '.stock', '.FIO', '.post', '.login'];
    var mass2 = ['.article', '.nomenclature', '.measure', '.needs', '.stock', '.agree', '.del-wrap', '.edit-wrap'];

    /*************************************/


    //	функция добавления и удаления инпутов
    function addRemoveInput(elem, val) {
        var x;
        var myInput;
        var mySpan;
        for (var i = 0; i < mass.length; i++) {
            x = elem.find(mass[i]);
            myInput = x.find('input');
            mySpan = x.find('span');
            if (val == 'add') {
				$('.numer-num').keypress(function(e) {
					if (!(e.which==8 || e.which==46   ||(e.which>47 && e.which<58))) return false;
				});
                myInput.show();
                myInput.val(mySpan.text());
                mySpan.hide();
                myInput.attr('data-input', myInput.val());
                myInput.removeAttr("disabled").addClass('edit-num');
            } else if (val == 'remove') {
                if (myInput.attr('data-input')) {
                    myInput.removeClass('edit-num').prop('disabled', 'disabled');
                    myInput.val(myInput.attr('data-input'));
                    mySpan.text(myInput.attr('data-input'));
                    myInput.removeAttr('data-input');
                    myInput.hide();
                    mySpan.show();
                }
            }
        }
    }

   

    //	редактирование построчное
    $('.edit').on('click', function() {
        var x;
        var myInput;
        var val;
        if ($(this).parents('.item').hasClass("edited")) {
        } else {
            var item = $(this).parents('.item');
            var max = $('.check').length;
            var max2 = $('.editItem').length;
            if (max > 0 || max2 > 0) {
                return false;
            } else {
                item.addClass('editItem');
                $('.edit').addClass('edit-group');
                $(this).removeClass('edit-group').addClass('active');
                item.find('.agr').css('display', 'inline-block');
                item.find('.admin-icon').css('display', 'none');
                $('.check-block input').prop('disabled', 'disabled').trigger('refresh');
                item.find('.check-block input').attr('checked', true).trigger('refresh');
                item.find('.delete').css('display', 'inline-block');
                addRemoveInput(item, 'add');
                item.find('.FIO input').removeAttr("disabled").addClass('edit-num');
                $('.statistic-total').css('background', '#b7b7b7');
                o.save_b.show();
                o.cancel_b.show();
                o.add_b.hide();
            }
        }
    });

    //	редактирование чекбоксом
    $('.check-block input').on('change', function() {
        var item;
        item = $(this).parents('.item');
        if (item.hasClass("edited")) {
        } else {
            if ($(this).is(':checked')) {
                item.addClass('check');
                $('.statistic-total').css('background', '#b7b7b7');
                // $(this).prop('checked', true).trigger('refresh');
                o.group_e_b.show();
                o.group_d_b.show();
                o.add_b.hide();
                initEditmenu();
            } else if ($('.check').length > 1) {
                initEditmenu();
                item.removeClass('check');
            } else {
                initEditmenu();
                item.removeClass('check');
                $('.statistic-total').css('background', 'white');
                o.group_e_b.hide();
                o.group_d_b.hide();
                o.add_b.show();
            }
        }
    });


    //	главный чекбокс
    $('.items-head input').on('change', function() {
        if ($('.item').hasClass('editItem')) {
            return false;
        }
        if ($(this).is(':checked')) {
            o.admin_i_l.each(function() {
                if ($(this).hasClass('edited')) {
                } else {
                    $(this).addClass('check');
                    $(this).find('.check-block input').attr('checked', true).trigger('refresh');
                    o.add_b.hide();
                    o.save_b.hide();
                    o.cancel_b.hide();
                    o.group_e_b.show();
                    o.group_d_b.show();
                    $('.statistic-total').css('background', '#b7b7b7');
                }

            });
            initEditmenu();
        } else {
            o.admin_i_l.each(function() {
                $(this).removeClass('check');
                $(this).find('.check-block input').attr('checked', false).trigger('refresh');
                o.add_b.show();
                o.save_b.hide();
                o.cancel_b.hide();
                o.group_e_b.hide();
                o.group_d_b.hide();
                $('.statistic-total').css('background', 'white');
            });
            initEditmenu();
        }

    });
    //	кнопка редактирования группы
    o.group_e_b.on('click', function() {
        $('.edit').addClass('edit-group');
        var item;
        $('.check').each(function() {
            item = $(this);
            item.addClass('editItem');
            item.find('.agr').css('display', 'inline-block');
            item.find('.edit').removeClass('edit-group').addClass('active');
            $('.check-block input').prop('disabled', 'disabled').trigger('refresh');
            item.find('.check-block input').attr('checked', true).trigger('refresh');
            $(this).find('.admin-icon').css('display', 'none');
            addRemoveInput(item, 'add');
            o.group_e_b.hide();
            o.group_d_b.hide();
            o.save_b.show();
            o.cancel_b.show();
            o.add_b.hide();

        })
    });

    //	кнопка cancel
    o.cancel_b.on('click', function() {
        var myInput;
        $('.item').each(function() {
            $(this).removeClass('editItem');
            $(this).removeClass('check');
            $(this).find('.agr').css('display', 'none');
            $(this).find('.admin-icon').css('display', 'block');
            $(this).find('.edit').removeClass('active').removeClass('edit-group');
            if ($(this).find('.check-block input').is(':checked')) {
                $(this).find('.check-block input').attr('checked', false).trigger('refresh');
            }
            $('.items-head').find('.check-block input').attr('checked', false).prop('disabled', '').trigger('refresh');

            if (!$(this).hasClass('edited')) {
                $(this).find('.check-block input').prop('disabled', '').trigger('refresh');
                addRemoveInput($(this), 'remove');
                o.save_b.hide();
                o.cancel_b.hide();
                $('.statistic-total').css('background', 'white');
                o.add_b.show();
            }
        });
    });

}//startMagic