<?
$MESS["SECURITY_USER_RECOVERY_CODES_PRINT_TITLE"] = "Reserve-Codes für Verifizierung";
$MESS["SECURITY_USER_RECOVERY_CODES_PRINT_CREATED"] = "Erstellt am: #DATE#";
$MESS["SECURITY_USER_RECOVERY_CODES_PRINT_NOTE"] = "Ein Code kann nur einmal benutzt werden. Hinweis: Bereits benutzte Codes können von der Liste gestrichen werden.";
?>