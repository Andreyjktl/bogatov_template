<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;


$arFilter = $arParams["FILTER"] ? $arParams["FILTER"] : array();
$arFields = $arParams["FIELDS"] ? $arParams["FIELDS"] : array();
$arFieldsSelect = $arParams["FIELDS_SELECT"] ? $arParams["FIELDS_SELECT"] : array();

$arFilter['!ID'] = $USER->GetID();

// выбираем пользователей
$rsUsers = CUser::GetList(($by="ID"), ($order="ASC"), $arFilter,
		array( 
			'SELECT' => $arFieldsSelect, 'FIELDS' => $arFields
		)
);


while ($arElement = $rsUsers->GetNext())
{		
	$arResult['USERS'][] = $arElement;
}	

$arResult["NAV_STRING"] = $rsUsers->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
$arResult["NAV_RESULT"] = $rsUsers;

$this->IncludeComponentTemplate();

?>