<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<form method="post" action="/ajax/users.php" class="ajax-form-update-users">
	<input type="hidden" id="element_id_firm" value="<?= $_REQUEST[ELEMENT_ID_FIRM] ?>" name="firm_id">
	<input type="hidden" value="update" name="action">
	<div class="items admin">
		<div class="items-head">
			<div class="head-row">
				<div class="check-block"><input type="checkbox"></div>
				<div class="org-num">№ п/п</div>
				<div class="FIO">ФИО</div>
				<div class="post">Должность</div>
				<div class="login">Логин</div>
				<div class="access">Доступ к справочнику информации</div>
				<div class="admin-menu"><div class="admin-icon"></div></div>
				<div class="edit-wrap">	</div>
			</div>	
		</div>
		<div class="items-list ajax-list-users">
		<? if( $arResult["USERS"] ) : ?>	
		<? $i = 1; foreach ($arResult["USERS"] as $res): ?>				
		<div class="item" id="ad<?= $res['ID'] ?>">
			<div class="check-block"><input name="check[<?= $res['ID'] ?>]" type="checkbox" value="1" ></div>
			<div class="org-num"><?= $i++ ?></div>
			<div class="FIO">
				<input type='text' disabled name="name[<?= $res['ID'] ?>]" value="<?= $res['LAST_NAME'] ?> <?= $res['NAME'] ?> <?= $res['SECOND_NAME'] ?>">
				<span><?= $res['LAST_NAME'] ?> <?= $res['NAME'] ?> <?= $res['SECOND_NAME'] ?></span>
			</div>
			<div class="post">
				<input type='text' name="personal_profession[<?= $res['ID'] ?>]"  disabled value="<?= $res['PERSONAL_PROFESSION'] ?>">	
				<span><?= $res['PERSONAL_PROFESSION'] ?></span>	
			</div>
			<div class="login">
				<input name="login[<?= $res['ID'] ?>]" type='text' disabled value="<?= $res['LOGIN'] ?>">
				<span><?= $res['LOGIN'] ?></span>	
			</div>
			<div class="access <?= $res['UF_ACCESS_DIRECTORY'] ? "checked" : ""?>"><div class="agr"><input name="access_directory[<?= $res['ID'] ?>]" type="checkbox" value="1"> </div></div>
			<div class="admin-menu">
				<div class="admin-icon">
					<ul>
						<li><a data-num="<?= $res['ID'] ?>" class="dell-it dell-fancy">Удалить</a></li>
						<li><a data-num="<?= $res['ID'] ?>"  class="changePass" href="#new-user-password">Сменить пароль</a></li>
					</ul>
				</div>
			</div>						
			<div class="edit-wrap"><div class="edit "></div></div>
		</div>
		<? endforeach; ?>
		<? else: ?>
		<p style="text-align: center;"><font style="">Нет данных</font></p>
		<? endif ?>
		</div>		
	</div>
	<div class='inditer'></div>
	<div class="statistic-total">					
			<button class="save-button" type="submit">Сохранить</button> 
			<button class="cancel-button" type="reset">Отменить</button>
			<button class="group-edit-button" type="reset"></button> 
			<button class="group-delete-button" data-form=".ajax-form-update-users" type="submit"></button> 					
	</div>
	</form>
	<?if ($arResult["NAV_RESULT"]->NavPageCount > 0): ?>
	<?=$arResult["NAV_STRING"]?>
	<? endif; ?>

	
	<div style="display:none">
		<div class="add-org-window" id="add-user-window">
			<span>ДОБАВИТЬ ПОЛЬЗОВАТЕЛЯ</span>
			<form method="post" action="/ajax/users.php" class="ajax-form-add-users">
				<input type="hidden" value="<?= $_REQUEST[ELEMENT_ID_FIRM] ?>" name="firm_id">
				<input type="hidden" value="add" name="action">
				<div class="formDiv">
					<label for="user-fio">ФИО</label>
					<input type="text" id="user-fio" name="name" >
				</div>
				<div class="formDiv">
					<label for="user-post">Должность</label>
					<input type="text" id="user-post" name="personal_profession" >
				</div>
				<div class="formDiv">
					<label for="user-login">Логин</label>
					<input type="text" id="user-login" name="login" placeholder="" >
				</div>
				<div class="formDiv">
					<label for="user-pass">Новый пароль</label>
					<input type="password" id="user-pass" name="pass" placeholder="" >
				</div>		
				<div class="formDiv">
					<label for="user-pass-confirm">Подтверждение пароля</label>
					<input type="password" id="user-pass-confirm" name="passv" placeholder="" >
				</div>
				<div class="formDiv">
					<label for="user-xml-id">Внешний код</label>
					<input type="text" id="user-pass-confirm" name="user-xml-id" placeholder="" >
				</div>	
				<div class="formDiv">						
					<input type="checkbox" value="1"  name="access_directory" class="checkbox"><div class="check-span">Доступ к «Справочнику информации»</div>
				</div>
				<div class="error"></div>
				<div class="formDiv">	
					<button class="org-save" type="submit">Сохранить изменения</button>	
					<button class="org-cancel" type="reset">Отменить</button>
				</div>			
			</form>
		</div>
	</div>
	<div style="display:none">
		<div class="add-org-window" id="new-user-password">
			<span>СМЕНИТЬ ПАРОЛЬ</span>
			<form method="post" action="/ajax/users.php" class="ajax-form-pass-users">
				<input type="hidden" value="<?= $_REQUEST[ELEMENT_ID_FIRM] ?>" name="firm_id">
				<input type="hidden" value="pass" name="action">
				<input id="update_user_pass" type="hidden" value="" name="user_id">
				<div class="formDiv">
					<label for="new-pass">Новый пароль</label>
					<input type="password" id="new-pass" name="pass" placeholder="" >
				</div>		
				<div class="formDiv">
					<label for="new-pass-confirm">Подтверждение пароля</label>
					<input type="password" id="new-pass-confirm" name="passv" placeholder="" >
				</div>	
				<div class="error"></div>
				<div class="formDiv">	
					<button class="org-save" type="submit">Сохранить изменения</button>	
					<button class="org-cancel" type="reset">Отменить</button>
				</div>			
			</form>
		</div>
	</div>
	<div style="display:none">
		<div class="popup-good" id="confirm-window">
			<div class="confirm">
			<span>Подтвердите действие</span>
			<div>
				<button class="dell" type="submit">Удалить</button>	
				<button class="canc" type="submit">Отменить</button>
			</div>
			</div>				
		</div>
	</div>