<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if( $arResult["USERS"] ) : ?>	
<? $i = 1; foreach ($arResult["USERS"] as $res): ?>				
<div class="item" id="ad<?= $res['ID'] ?>">
	<div class="check-block"><input name="check[<?= $res['ID'] ?>]" type="checkbox" value="1" ></div>
	<div class="org-num"><?= $i++ ?></div>
	<div class="FIO">
		<input type='text' disabled name="name[<?= $res['ID'] ?>]" value="<?= $res['LAST_NAME'] ?> <?= $res['NAME'] ?> <?= $res['SECOND_NAME'] ?>">
		<span><?= $res['LAST_NAME'] ?> <?= $res['NAME'] ?> <?= $res['SECOND_NAME'] ?></span>
	</div>
	<div class="post">
		<input type='text' name="personal_profession[<?= $res['ID'] ?>]"  disabled value="<?= $res['PERSONAL_PROFESSION'] ?>">	
		<span><?= $res['PERSONAL_PROFESSION'] ?></span>	
	</div>
	<div class="login">
		<input name="login[<?= $res['ID'] ?>]" type='text' disabled value="<?= $res['LOGIN'] ?>">
		<span><?= $res['LOGIN'] ?></span>	
	</div>
	<div class="access <?= $res['UF_ACCESS_DIRECTORY'] ? "checked" : ""?>"><div class="agr"><input name="access_directory[<?= $res['ID'] ?>]" type="checkbox" value="1"> </div></div>
	<div class="admin-menu">
		<div class="admin-icon">
			<ul>
			<li><a data-num="<?= $res['ID'] ?>" class="dell-it dell-fancy">Удалить</a></li>
						<li><a data-num="<?= $res['ID'] ?>"  class="changePass" href="#new-user-password">Сменить пароль</a></li>
			</ul>
		</div>
	</div>						
	<div class="edit-wrap"><div class="edit "></div></div>
</div>
<? endforeach; ?>
<? else: ?>
<p style="text-align: center;"><font style="">Нет данных</font></p>
<? endif ?>