<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<div class="profile-add">
	<span>ИНФОРМАЦИЯ ОРГАНИЗАЦИИ</span>
	<? if ( $USER->IsAdmin() ) : ?>
	<a class="add-prof" href="/ajax/export.php">Выгрузить таблицу</a>
	<? endif ?>
</div>
<form method="post" action="/ajax/reference_information.php" class="ajax-form-update-reference-information">
	<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
	<input type="hidden" value="update" name="action">
	<? if($arParams["ACCESS_DIRECTORY"] == "Y") : ?>
	<div class="items admin">
		<div class="items-head">
			<div class="head-row">
				<div class="check-block"><input type="checkbox"> </div>
				<div class="article">Артикул</div>
				<div class="nomenclature">Номенклатура</div>
				<div class="measure">Ед.упаковки</div>
				<div class="">Мин.заказ</div>
				<div class="">Расчитанный заказ</div>
				<div class="needs">Требуется заказать</div>
				<div class="">Ожидаемая дата поступления</div>
				<div class="check-block"><input type="checkbox"> </div>
				
				
				<?/*<div class="agree">Согласованно </div>*/?>
				
				<div class="edit-wrap"></div>
			</div>	
		</div>
		<div class="items-list ajax-list-users-reference-information">
		
		<? $i = 1; ?>
		<? if (count($arResult["ITEMS"]) > 0): ?>
			<? foreach ($arResult["ITEMS"] as $arElement): ?>
			<? ;// echo "<pre>"; var_dump($arElement); echo "</pre>" ?>
			<div class="item <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "edited" : "" ?>" id="as<?= $arElement["ID"]?>">
				<div class="check-block"><input value="1" name="check[<?= $arElement["ID"]?>]" type="checkbox" <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "disabled" : "" ?> ></div>
				<div class="article"><?= $arElement['PROPERTIES']['ARTICLE']['VALUE'] ?></div>
				<div class="nomenclature"><?= $arElement['NAME'] ?></div>
				<div class="measure"><?= $arElement['PROPERTIES']['UNITS']['VALUE'] ?></div>
				<div class="">Мин.заказ</div>
				<div class="">Расчитанный заказ</div>
				<div class="needs">
					<span><?= number_format($arElement['PROPERTIES']['NEED']['VALUE'],3,".","") ?></span>
					<input class="numer-num" type='text' name="need[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['NEED']['VALUE'] ?>' disabled>
				</div>
				<div class="stock">
					<span><?= number_format($arElement['PROPERTIES']['RESERVE']['VALUE'], 3, ".","") ?></span>
					<input class="numer-num" type='text' name="reserve[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['RESERVE']['VALUE'] ?>' disabled>
				</div>
				<?/*<div class="agree <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "checked" : "" ?>">					
					<? if ( $USER->IsAdmin() ) : ?>
					<? if(  $arElement['PROPERTIES']['FAGREE']['VALUE'] == 1 ) : ?>					
					<? else: ?>
					<input  name="fagree[<?= $arElement["ID"]?>]" type="checkbox" value="1">
					<? endif ?>
					<? endif ?>			
				</div>		*/?>	
				<div class="edit-wrap"><div class="edit"></div></div>
			</div>
			<? endforeach ?>
		<? else: ?>
		<p style="text-align: center;"><font style="">Нет данных</font></p>
		<? endif ?>	
		</div>
	</div>	
	<div class='inditer'></div>
	<div class="statistic-total">
		<button class="add-button"  href="#popup-numen" type="reset">Добавить номенклатурную позицию из каталога</button>
		<button class="save-button" type="submit" >Сохранить</button> 
		<button class="cancel-button" type="reset">Отменить</button>
		<button class="group-edit-button" type="reset" ></button> 
		<button class="group-delete-button"  data-form=".ajax-form-update-reference-information" type="submit"></button> 					
	</div>
	<? else : ?>
	<p style="text-align: center;"><font style="">Нет доступа</font></p>
	<? endif ?>
</form>

<div style="display:none">
	<div class="popup-numen" id="popup-numen">
		<span>ДОБАВИТЬ НОМЕНКЛАТУРУ</span>
		<form method="post" action="/ajax/reference_information.php" class="ajax-form-add-reference-information">
			<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
			<input type="hidden" value="add" name="action">
			<div class="numen-table">				
				<div class="numen-items ">
					<div class="numen-items-head">
						<div class="numen-head-row">
							<div class="numen-check-block"> </div>
							<div class="article">Артикул</div>
							<div class="nomenclature">Номенклатура</div>
							<div class="measure">Ед.изм-я</div>
						</div>
					</div>
					<div class="numen-items-list ajax-catalog-items">
						
					</div>	
				</div>
			</div>
			<button class="numen-add-num-button" type="submit">Добавить номенклатурную позицию из каталога</button>
			<button class="numen-cancel-button" onclick=' parent.$.fancybox.close();' type="reset" >Отменить</button>
		</form>
	</div>
</div>
<? if (strlen($arResult["NAV_STRING"]) > 0):?><?= $arResult["NAV_STRING"] ?><?endif?>