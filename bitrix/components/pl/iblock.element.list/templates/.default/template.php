<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<div class="ajax-list-firm">
<div class="items">
	<div class="items-head">
		<div class="head-row">
			<div class="org-num">№ п/п</div>
			<div class="org">Организация</div>
			<div class="del-item"></div>
		</div>
	</div>	
	<div class="items-list">
	<? 	$i = $arResult["NAV_PAGE_NUM"]; ?>
	<? if ($arResult["ITEMS"]): ?>
		<? foreach ($arResult["ITEMS"] as $arElement): ?>		
		<div class="item">
			<div class="org-num"><?= $i++ ?></div>
			<div class="org"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></div>
			<? if ($arResult["CAN_DELETE"] == "Y"):?>
				<?if ($arElement["CAN_DELETE"] == "Y"):?>
			<div class="del-item">
				<a class="delete-org" ref="<?=$arElement["DELETE_LINK"]?>" onclick=""></a>
			</div>
				<? endif ?>
			<? endif ?>
		</div>		
		<? endforeach ?>		
	<? else: ?>
	<p style="text-align: center">Нет данных</p>
	<? endif ?>
	</div>	
</div>	
<? if (strlen($arResult["NAV_STRING"]) > 0):?><?= $arResult["NAV_STRING"] ?><?endif?>
</div>

<!-- Форма "Добавить организацию" -->
<div style="display:none">
	<div class="add-org-window" id="add-org-window">
		<span>ДОБАВИТЬ ОРГАНИЗАЦИЮ</span>
		<form method="post" action="/ajax/firms.php" class="ajax-form-add-frim">
			<div class="formDiv">
				<label for="org-name">Название организации</label>
				<input type="text" id="org-name" name="org-name" >
			</div>				
			<div class="formDiv">
				<label for="org-xml-id">Внешний код</label>
				<input type="text" id="org-name" name="org-xml-id" >
			</div>
			<div class="error"></div>
			<button class="org-save" type="submit">Сохранить изменения</button>	
			<button type="reset" class="org-cancel">Отменить</button>					
		</form>
	</div>
</div>