<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<div class="profile-add">
	<? if ( $USER->IsAdmin() ) : ?>
	<a class="add-prof" href="/ajax/export.php">Выгрузить таблицу</a>
	<? endif ?>
</div>
<form method="post" action="/ajax/orders_all.php" class="ajax-form-update-reference-information">
	<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
	<input type="hidden" value="update" name="action">
	<? if($arParams["ACCESS_DIRECTORY"] == "Y") : ?>
	<div class="items admin">
		<div class="items-head">
			<div class="head-row">
				<div class="nomenclature">Номенклатура</div>
				<div class="headname">Срок логистики</div>
				<div class="headname">Минимальный заказ</div>
				<div class="headname">Единица упаковки</div>

<?/*
				<div class="headname">Период расчета</div>
				<div class="headname">Согласован заказ</div>

				<div class="headname">Дата размещения</div>
				<div class="headname">Кол-во</div>

				<div class="headname">Дата поступления</div>
				<div class="headname">Кол-во</div>
*/?>

			</div>	
		</div>
		<div class="items-list ajax-list-users-reference-information">
		
		<? $i = 1; ?>
		<? if (count($arResult["ITEMS"]) > 0): ?>
			<? foreach ($arResult["ITEMS"] as $arElement): ?>

			<? //;echo "<pre>"; var_dump($arElement); echo "</pre>" ?>
		
			<div class="item <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "edited" : "" ?>" id="as<?= $arElement["ID"]?>">
				<div class="nomenclature"><a href=<?=$arElement["DETAIL_PAGE_URL"]?>><?= $arElement['NAME'] ?></a></div>

				<div class="headname"><?= $arElement['PROPERTIES']['SV_LOGISTIC']['VALUE'] ?></div>
				<div class="headname"><?= $arElement['PROPERTIES']['SV_PERIOD']['VALUE'] ?></div>
				<div class="headname"><?= $arElement['PROPERTIES']['SV_UPAK']['VALUE'] ?></div>

<?/*
								<div class="agree <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "checked" : "" ?>">					
					<? if ( $USER->IsAdmin() ) : ?>
					<? if(  $arElement['PROPERTIES']['FAGREE']['VALUE'] == 1 ) : ?>					
					<? else: ?>
					<input  name="fagree[<?= $arElement["ID"]?>]" type="checkbox" value="1">
					<? endif ?>
					<? endif ?>			
				</div>		
*/?>	
				</div>
			<? endforeach ?>
		<? else: ?>
		<p style="text-align: center;"><font style="">Нет данных</font></p>
		<? endif ?>	
		</div>
	</div>	
	
	<? else : ?>
	<p style="text-align: center;"><font style="">Нет доступа</font></p>
	<? endif ?>
</form>

<? if (strlen($arResult["NAV_STRING"]) > 0):?><?= $arResult["NAV_STRING"] ?><?endif?>