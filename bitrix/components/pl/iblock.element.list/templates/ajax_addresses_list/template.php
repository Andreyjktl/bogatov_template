<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>

<? $i = 1; ?>
<? if (count($arResult["ITEMS"]) > 0): ?>
	<? foreach ($arResult["ITEMS"] as $arElement): ?>	
	<div class="item-addr">						
		<div class="org-num"><?= $i++ ?></div>
		<div class="delivery-adress"><?= $arElement['NAME'] ?></div>	
		<div class="del-item"><a href="#order" data-num="<?= $arElement['ID'] ?>" class="delete-address del-item"></a></div>						
	</div>
	<? endforeach ?>
<? else: ?>
<p style="text-align: center;"><font style="">Нет данных</font></p>
<? endif ?>		