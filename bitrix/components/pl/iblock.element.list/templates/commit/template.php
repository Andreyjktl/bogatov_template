<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<div class="profile-add">
	<? if ( $USER->IsAdmin() ) : ?>
	<a class="add-prof" href="/ajax/export.php">Выгрузить таблицу</a>
	<? endif ?>
</div>
<form method="post" action="/ajax/commit.php" class="ajax-form-update-reference-information">
	<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
	<input type="hidden" value="update" name="action">
	<? if($arParams["ACCESS_DIRECTORY"] == "Y") : ?>
	<div class="items admin">
		<div class="items-head">
			<div class="head-row">
				
				<div class="">Номенклатура</div>
				
				<div class="headname">Единица упаковки</div>
				<div class="headname">Минимальный заказ</div>
				<div class="headname">Дефицит товарных запасов</div>
				<div class="headname">Требуется заказать</div>
				<div class="headname">Ожидаемая дата поставки</div>
				<div class="headname">Дата согласования</div>
				<div class="headname">Ответственное лицо</div>

				<div class="headname">Заказано <br> производителю</div>
				<div class="headname">Ожидаемая дата поставки</div>
				<div class="headname">Дата согласования</div>
				<div class="headname">Ответственное лицо</div>

				<?/*<div class="agree">Согласованно </div>*/?>
				
				<div class="edit-wrap"></div>
			</div>	
		</div>
		<div class="items-list ajax-list-users-reference-information">
		
		<? $i = 1; ?>
		<? if (count($arResult["ITEMS"]) > 0): ?>
			<? foreach ($arResult["ITEMS"] as $arElement): ?>
			<? ;// echo "<pre>"; var_dump($arElement); echo "</pre>" ?>
			<div class="item <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "edited" : "" ?>" id="as<?= $arElement["ID"]?>">
				
				<div class=""><a href="/profile/orders_all/index.php"><?= $arElement['NAME'] ?></a></div>
				<div class="headname270"><?= $arElement['PROPERTIES']['SV_UPAK']['VALUE'] ?></div>
				<div class="headname270"><?= $arElement['PROPERTIES']['SV_PERIOD']['VALUE'] ?></div>
				<div class="headname270"><?= $arElement['PROPERTIES']['ZAKAZ_F']['VALUE'] ?></div>

				<? //Данные покупателя ?>
				<div class="headname270"><?= $arElement['PROPERTIES']['ZAKAZ_S']['VALUE'] ?>	</div>

				<div class="headname270"><?= $arElement['PROPERTIES']['LOGISTIC']['VALUE'] ?></div>
				<div class="headname270"> <?= $arElement['PROPERTIES']['CONFIRM_DATE']['VALUE'] ?></div>
				<div class="headname270"><?= $arElement['PROPERTIES']['PERSON']['VALUE'] ?></div>

				<? //Данные поcтавщика ?>
				<div class="headname270"><?= $arElement['PROPERTIES']['ZAKAZ_T']['VALUE'] ?></div>

				<div class="headname270"><?= $arElement['PROPERTIES']['SHIP_DATE']['VALUE'] ?></div>
				
				<div class="headname270"> <?= $arElement['PROPERTIES']['CONFIRM_DATE_BG']['VALUE'] ?></div>
				
				<div class="headname270"><?= $arElement['PROPERTIES']['PERSON_BG']['VALUE'] ?></div>

			
				<div class="edit-wrap"><div class="edit"></div></div>
			</div>
			<? endforeach ?>
		<? else: ?>
		<p style="text-align: center;"><font style="">Нет данных</font></p>
		<? endif ?>	
		</div>
	</div>	
	<div class='inditer'></div>
	<div class="statistic-total">
		
		<button class="save-button" type="submit" >Сохранить</button> 
		<button class="cancel-button" type="reset">Отменить</button>
		<button class="group-edit-button" type="reset" ></button> 
		<button class="group-delete-button"  data-form=".ajax-form-update-reference-information" type="submit"></button> 					
	</div>
	
	<? else : ?>
	<p style="text-align: center;"><font style="">Нет доступа</font></p>
	<? endif ?>
</form>

<div style="display:none">
	<div class="popup-numen" id="popup-numen">
		
	</div>
</div>
<? if (strlen($arResult["NAV_STRING"]) > 0):?><?= $arResult["NAV_STRING"] ?><?endif?>