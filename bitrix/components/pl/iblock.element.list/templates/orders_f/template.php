<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>

<?/*<div class="profile-add">
	
	<a class="add-prof" href="/ajax/export.php">Выгрузить таблицу</a>
	
</div>*/?>

<form method="post" action="/ajax/orders_f.php" class="ajax-form-update-reference-information">
	<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
	<input type="hidden" value="update" name="action">
	<? if($arParams["ACCESS_DIRECTORY"] == "Y") : ?>
	<div class="items admin">
		<div class="items-head">
			<div class="head-row">
				<div class="check-block"><input type="checkbox"> </div>
				<div class="period">Период</div>
				<div class="nomenclature">Номенклатура</div>
				<div class="client_balance_c">Фактический <br> остаток <br> клиента</div>
				<div class="date" title="Данные поставщика">Дата <br> поступления</div>
				<div class="item_in" title="Данные поставщика">Поступившее <br> кол-во</div>
				<div class="amount" title="Данные поставщика">Доступный <br> остаток</div>
				<div class="client_storage_in_c">Ожидаемый <br> приход<br> на склад <br> клиента<br>(прочее)</div>
				<div class="needs">Ожидаемый <br> расход со <br> склада клиента<br>(потребность)</div>
				<div class="stock">Страховой запас</div>
				<div class="end_period">Расчетный <br> остаток на <br> конец <br> периода</div>
				<div class="zakaz">Дефицит <br> товарных <br> запасов<br>(Требуется заказать)</div>

				<?/*<div class="agree">Согласованно </div>*/?>
				
				<div class="edit-wrap"></div>
			</div>	
		</div>
		
		<div class="items-list ajax-list-users-reference-information">
		
		<? $i = 1; ?>
		<? if (count($arResult["ITEMS"]) > 0): ?>
			<? foreach ($arResult["ITEMS"] as $arElement): ?>

			<? if ($arElement['PROPERTIES']['CML2_LINK']['~VALUE'] == $GLOBALS['arProd']): ?>
			<? $arElement['PROPERTIES']['CML2_LINK']['~VALUE'] ?>
			
			<? ;// echo "<pre>"; var_dump($arElement); echo "</pre>" ?>
			<div class="item <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "edited" : "" ?>" id="as<?= $arElement["ID"]?>">
				<div class="check-block"><input value="1" name="check[<?= $arElement["ID"]?>]" type="checkbox" <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "disabled" : "" ?> ></div>
				<div class="period"><?= $arElement['PROPERTIES']['PERIOD']['VALUE'] ?></div>
				<div class="nomenclature"><?= $arElement['NAME'] ?></div>
				<div class="client_balance_c">
							<span><?= $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE']?></span>
					<input class="numer-num" type='text' name="client_balance[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] ?>' disabled>
				
				</div>
				<div class="date"><?= $arElement['PROPERTIES']['DATE_IN']['VALUE'] ?></div>
				<div class="item_in"><?= $arElement['PROPERTIES']['ITEM_IN']['VALUE'] ?></div>
				
				<div class="amount"><?= $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] ?></div>
				<div class="client_storage_in_c">
						<span><?= $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE']?></span>
					<input class="numer-num" type='text' name="client_storage_in[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE'] ?>' disabled>
				</div>
				<div class="needs">
							<span><?= $arElement['PROPERTIES']['NEED']['VALUE'] ?></span>
					<input class="numer-num" type='text' name="need[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['NEED']['VALUE'] ?>' disabled>
				
				</div>
				<div class="stock">
					<span><?= $arElement['PROPERTIES']['RESERVE']['VALUE']?></span>
					<input class="numer-num" type='text' name="reserve[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['RESERVE']['VALUE'] ?>' disabled>
				</div> 
				
				<div class="end_period">
				<?= $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] + $arElement['PROPERTIES']['ITEM_IN_D']['VALUE'] + $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] + $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE'] - $arElement['PROPERTIES']['NEED']['VALUE'] ?>
				</div>
				<div class="zakazf">
				<? if (number_format($arElement['PROPERTIES']['NEED']['VALUE'] + $arElement['PROPERTIES']['RESERVE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] - $arElement['PROPERTIES']['ITEM_IN_D']['VALUE'] - $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE']) > 0):?>
				<span class="numer-num" type='text' name="need[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['NEED']['VALUE'] + $arElement['PROPERTIES']['RESERVE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] - $arElement['PROPERTIES']['ITEM_IN_D']['VALUE'] - $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE'] ?>'>
				<?= $arElement['PROPERTIES']['NEED']['VALUE'] + $arElement['PROPERTIES']['RESERVE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] - $arElement['PROPERTIES']['ITEM_IN_D']['VALUE'] - $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE'] ?>
				</span>
				<?endif?>
				</div>

								
				<?/*<div class="agree <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "checked" : "" ?>">					
					<? if ( $USER->IsAdmin() ) : ?>
					<? if(  $arElement['PROPERTIES']['FAGREE']['VALUE'] == 1 ) : ?>					
					<? else: ?>
					<input  name="fagree[<?= $arElement["ID"]?>]" type="checkbox" value="1">
					<? endif ?>
					<? endif ?>			
				</div>		*/?>	
				<div class="edit-wrap"><div class="edit"></div></div>
			</div>
			<? endforeach ?>
		<? else: ?>
		<p style="text-align: center;"><font style="">Нет данных</font></p>
		<? endif ?>	
		</div>
	</div>	
	
	<div class='inditer'></div>
	<div class="statistic-total">
		<button class="add-button"  href="#popup-numen" type="reset">Добавить номенклатурную позицию из каталога</button>
		<button class="save-button" type="submit" >Сохранить</button> 
		<button class="cancel-button" type="reset">Отменить</button>
		<button class="group-edit-button" type="reset" ></button> 
		<button class="group-delete-button"  data-form=".ajax-form-update-reference-information" type="submit"></button> 					
	</div>
	<? else : ?>
	<p style="text-align: center;"><font style="">Нет доступа</font></p>
	<? endif ?>
</form>

<div style="display:none">
	<div class="popup-numen" id="popup-numen">
		<span>ДОБАВИТЬ НОМЕНКЛАТУРУ</span>
		<form method="post" action="/ajax/orders_f.php" class="ajax-form-add-reference-information">
			<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
			<input type="hidden" value="add" name="action">
			<div class="numen-table">				
				<div class="numen-items ">
					<div class="numen-items-head">
						<div class="numen-head-row">
							<div class="numen-check-block"> </div>
							<div class="">Период</div>
							<div class="article">Артикул</div>
							<div class="nomenclature">Номенклатура</div>
							<div class="measure">Ед.изм-я</div>
						</div>
					</div>
					<div class="numen-items-list ajax-catalog-items">
						
					</div>	
				</div>
			</div>
			<button class="numen-add-num-button" type="submit">Добавить номенклатурную позицию из каталога</button>
			<button class="numen-cancel-button" onclick=' parent.$.fancybox.close();' type="reset" >Отменить</button>
		</form>
	</div>
</div>
<? if (strlen($arResult["NAV_STRING"]) > 0):?><?= $arResult["NAV_STRING"] ?><?endif?>