<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<div class="profile-add">
	<? if ( $USER->IsAdmin() ) : ?>
	<a class="add-prof" href="/ajax/export.php">Выгрузить таблицу</a>
	<? endif ?>
</div>
<form method="post" action="/ajax/orders_s.php" class="ajax-form-update-reference-information">
	<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
	<input type="hidden" value="update" name="action">
	<? if($arParams["ACCESS_DIRECTORY"] == "Y") : ?>
	<div class="items admin">
		<div class="items-head">
			<div class="head-row">
				
				<div class="nomenclature">Номенклатура</div>
				
				<div class="item_pack">Единица упаковки</div>
				<div class="min_order">Минимальный заказ</div>
				<div class="zakaz_f">Дефицит товарных запасов</div>
				<div class="zakaz_s">Требуется заказать</div>
				<div class="logistic">Ожидаемая дата поставки</div>
				<div class="confirm_date">Дата <br> согласования</div>
				<div class="person">Ответственное лицо</div>

				<?/*<div class="agree">Согласованно </div>*/?>
				
				<div class="edit-wrap"></div>
			</div>	
		</div>
		<div class="items-list ajax-list-users-reference-information">
		
		<? $i = 1; ?>
		<? if (count($arResult["ITEMS"]) > 0): ?>
			<? foreach ($arResult["ITEMS"] as $arElement): ?>
			<? ;// echo "<pre>"; var_dump($arElement); echo "</pre>" ?>
			<div class="item <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "edited" : "" ?>" id="as<?= $arElement["ID"]?>">
				
				<div class="nomenclature"><?= $arElement['NAME'] ?></div>
				<div class="item_pack"><?= $arElement['PROPERTIES']['ITEM_PACK']['VALUE'] ?></div>
				<div class="min_order"><?= $arElement['PROPERTIES']['MIN_ORDER']['VALUE'] ?></div>
				<div class="zakaz_f"><?= $arElement['PROPERTIES']['ZAKAZ_F']['VALUE'] ?></div>

				<div class="zakaz_s">
					<span><?= number_format($arElement['PROPERTIES']['ZAKAZ_S']['VALUE'], 3, ".","") ?></span>
					<input class="numer-num" type='text' name="zakazs[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['ZAKAZ_S']['VALUE'] ?>' disabled>
				</div>

				<div class="logistic"><?= $arElement['PROPERTIES']['LOGISTIC']['VALUE'] ?></div>
				
				<div class="confirm_date"><?= $arElement['PROPERTIES']['CONFIRM_DATE']['VALUE'] ?></div>
				
				<div class="person"><?= $arElement['PROPERTIES']['PERSON']['VALUE'] ?></div>

								
	
				<div class="edit-wrap"><div class="edit"></div></div>
			</div>
			<? endforeach ?>
		<? else: ?>
		<p style="text-align: center;"><font style="">Нет данных</font></p>
		<? endif ?>	
		</div>
	</div>	
	<div class='inditer'></div>
	<div class="statistic-total">
	
		<button class="save-button" type="submit" >Сохранить</button> 
		<button class="cancel-button" type="reset">Отменить</button>
		<button class="group-edit-button" type="reset" ></button> 
		<button class="group-delete-button"  data-form=".ajax-form-update-reference-information" type="submit"></button> 					
	</div>
	<? else : ?>
	<p style="text-align: center;"><font style="">Нет доступа</font></p>
	<? endif ?>
</form>

<div style="display:none">
	<div class="popup-numen" id="popup-numen">
		
	</div>
</div>
<? if (strlen($arResult["NAV_STRING"]) > 0):?><?= $arResult["NAV_STRING"] ?><?endif?>