<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>


<div class="items">
	<div class="items-head">
		<div class="head-row">
			<div class="org-num">№ п/п</div>
			<div class="delivery-adress">Адрес доставки</div>
			<div class="del-item"></div>
		</div>
	</div>			
	<div class="items-list ajax-list-address">
	<? $i = 1; ?>
	<? if (count($arResult["ITEMS"]) > 0): ?>
		<? foreach ($arResult["ITEMS"] as $arElement): ?>	
		<div class="item-addr">						
			<div class="org-num"><?= $i++ ?></div>
			<div class="delivery-adress"><?= $arElement['NAME'] ?></div>	
			<div class="del-item"><a href="#order" data-num="<?= $arElement['ID'] ?>" class="delete-address del-item"></a></div>						
		</div>
		<? endforeach ?>
	<? else: ?>
	<p style="text-align: center;"><font style="">Нет данных</font></p>
	<? endif ?>		
	</div>
</div>
<? if (strlen($arResult["NAV_STRING"]) > 0):?><?= $arResult["NAV_STRING"] ?><?endif?>

<div style="display:none">
	<div class="add-org-window" id="new-address-form">
		<span>ДОБАВИТЬ АДРЕС ДОСТАВКИ</span>
		<form method="post" action="/ajax/addresses.php"  class="ajax-form-add-address">
			<input type="hidden" value="<?= $_REQUEST[ELEMENT_ID_FIRM] ?>" name="firm_id">
			<input type="hidden" value="add" name="action">
			<div class="formDiv">
				<label for="new-address">Адрес</label>
				<input type="text" id="new-address" name="address" placeholder="" >
			</div>	
			<div class="error"></div>
			<div class="formDiv">	
				<button class="org-save" type="submit">Сохранить изменения</button>	
				<button class="org-cancel" type="reset">Отменить</button>
			</div>			
		</form>
	</div>
</div>