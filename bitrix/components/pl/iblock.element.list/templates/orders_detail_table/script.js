BX.ready(function() {  
    $('.add-button').fancybox({		
        openEffect: 'fade',
        closeEffect: 'fade',
        prevEffect: 'none',
        nextEffect: 'none',
        centerOnScroll: 'false',
        scrolling: 'no',
        fitToView: false,
        scrollOutside: true,
        padding: 0,
        top: 50,
        tpl: {
            closeBtn: '<a title="Close" class="fancybox-item order-close fancybox-close gallery-close" href="javascript:;"></a>'
        },
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.60)'
                }
            }

        },
        beforeLoad: function() {
            $('.ajax-catalog-items').html('');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/ajax/catalog.php',
                data: {firm_id: $('#element_id_firm').val() },
                success: function(res){					
                    $('.ajax-catalog-items').html(res.content);
                    startMagic();
                }
             });
        },
      
    });
})
