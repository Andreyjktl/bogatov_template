<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>

<?// Фильтр по прошедшим периодам?>
<?php
$today = date("d.m.y");
?>
<div style = "float: left";>Сегодня: <?= $today ?>   </div>
<div style = "float: left";>   Показывать данные предыдущих периодов</div>
<div> <input  name="period_check" type="checkbox" value="1"> </div>

<?// Конец.Фильтр по прошедшим периодам?>


<form method="post" action="/ajax/orders_f.php" class="ajax-form-update-reference-information">
	<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
	<input type="hidden" value="update" name="action">
	<? if($arParams["ACCESS_DIRECTORY"] == "Y") : ?>
	<div class="detailHeader"> <h3>Первичный расчет заявок</h3></div>
	


	<div class="items admin">
		<div class="items-head">
			<div class="head-row">
				<div class="period">Период</div>
				<div class="client_value">Фактический остаток клиента</div>
				<div class="amount" title="Данные поставщика">Доступный остаток на начало периода</div>
				<div class="date" title="Данные поставщика">Дата поступления</div>
				<div class="item_in" title="Данные поставщика">Поступившее кол-во</div>
				<div class="storage_in">Ожидаемый приход на склад клиента<br>(прочее)</div>
				<div class="needs">Ожидаемый расход со склада клиента<br>(потребность)</div>
				<div class="stock">Страховой запас</div>
				<div class="end_period">Расчетный остаток на конец периода</div>
				<div class="zakaz">Дефицит товарных запасов<br>(Требуется заказать)</div>
				
				<div class="edit-wrap"></div>
			</div>	
		</div>
		
		<div class="items-list ajax-list-users-reference-information">
		
		<? $i = 1; ?>
		
		<? if (count($arResult["ITEMS"]) > 0): ?>
			<? foreach ($arResult["ITEMS"] as $arElement): ?>

			<? if ($arElement['PROPERTIES']['CML2_LINK']['~VALUE'] == $GLOBALS['arProd']): ?>
			<? $arElement['PROPERTIES']['CML2_LINK']['~VALUE'] ?>

			<? ; //echo "<pre>"; var_dump($arElement); echo "</pre>" ?>

			<div class="item <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "edited" : "" ?>" id="as<?= $arElement["ID"]?>">
				<div style="border-right: 1px solid #D2D2D2;"  class="period"><?= $arElement['PROPERTIES']['PERIOD']['VALUE'] ?></div>
					<div style="border-right: 1px solid #D2D2D2;" class="client_balance_c">
							<span><?= $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE']?></span>
					<input class="numer-num" type='text' name="client_balance[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] ?>' disabled>
				
				</div>
				<div style="border-right: 1px solid #D2D2D2;"  class="amount"><?= $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] ?></div>
				<div style="border-right: 1px solid #D2D2D2;"  class="date"><?= $arElement['PROPERTIES']['DATE_IN']['VALUE'] ?></div>
				<div style="border-right: 1px solid #D2D2D2;"  class="item_in"><?= $arElement['PROPERTIES']['ITEM_IN']['VALUE'] ?></div>
				
				
				<div style="border-right: 1px solid #D2D2D2;"  class="client_storage_in_c">
						<span><?= $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE']?></span>
					<input class="numer-num" type='text' name="client_storage_in[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE'] ?>' disabled>
				</div>
				<div style="border-right: 1px solid #D2D2D2;"  class="needs">
							<span><?= $arElement['PROPERTIES']['NEED']['VALUE'] ?></span>
					<input class="numer-num" type='text' name="need[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['NEED']['VALUE'] ?>' disabled>
				
				</div>
				<div style="border-right: 1px solid #D2D2D2;"  class="stock">
					<span><?= $arElement['PROPERTIES']['RESERVE']['VALUE']?></span>
					<input class="numer-num" type='text' name="reserve[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['RESERVE']['VALUE'] ?>' disabled>
				</div> 
				
				<div style="border-right: 1px solid #D2D2D2;"  class="end_period">
				<?= $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] + $arElement['PROPERTIES']['ITEM_IN_D']['VALUE'] + $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] + $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE'] - $arElement['PROPERTIES']['NEED']['VALUE'] ?>
				</div>
				<div class="zakaz_f">
				<? if (number_format($arElement['PROPERTIES']['NEED']['VALUE'] + $arElement['PROPERTIES']['RESERVE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] - $arElement['PROPERTIES']['ITEM_IN_D']['VALUE'] - $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE']) > 0):?>
				<?= $arElement['PROPERTIES']['NEED']['VALUE'] + $arElement['PROPERTIES']['RESERVE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] - $arElement['PROPERTIES']['ITEM_IN_D']['VALUE'] - $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE'] ?>
				<?endif?>
				</div>
				

				<div class="edit-wrap"><div class="edit"></div></div>
			</div>
					<? endif ?>

			<? endforeach ?>
		<? else: ?>
		<p style="text-align: center;"><font style="">Нет данных</font></p>
		<? endif ?>	
		</div>
	</div>	
	<div class='inditer'></div>
	<div class="order-total">
		<?//<button class="add-button"  href="#popup-numen" type="reset">Добавить заявку</button>?>
		<button class="save-button" type="submit" >Сохранить</button> 
		<button class="cancel-button" type="reset">Отменить</button>
		<button class="group-edit-button" type="reset" ></button> 
		<button class="group-delete-button"  data-form=".ajax-form-update-reference-information" type="submit"></button> 					
	</div>
	<? else : ?>
	<p style="text-align: center;"><font style="">Нет доступа</font></p>
	<? endif ?>

	<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
	<input type="hidden" value="update" name="action">
	<? if($arParams["ACCESS_DIRECTORY"] == "Y") : ?>
	<div class="detailHeader"> <h3>Вторичный расчет заявок</h3></div>
	<div class="items admin">
		<div class="items-head">
			<div class="head-row">
				<div class="period">Период</div>
				<div class="headname">Дефицит товарных запасов</div>
				<div class="headname">Требуется заказать</div>
				<div class="headname">Ожидаемая дата поставки</div>
				<div class="headname">Дата <br> согласования</div>
				<div class="headname">Ответственное лицо</div>

				<div class="agree">Согласованно</div>
				
				<div class="edit-wrap"></div>
			</div>	
		</div>
		<div class="items-list ajax-list-users-reference-information">
		
		<? $i = 1; ?>
		<? if (count($arResult["ITEMS"]) > 0): ?>
			<? foreach ($arResult["ITEMS"] as $arElement): ?>

			<? if ($arElement['PROPERTIES']['CML2_LINK']['~VALUE'] == $GLOBALS['arProd']): ?>
			<? $arElement['PROPERTIES']['CML2_LINK']['~VALUE'] ?>

			<?// ;echo "<pre>"; var_dump($arElement); echo "</pre>" ?>
			<div class="item <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "edited" : "" ?>" id="as<?= $arElement["ID"]?>">
				<div style="border-right: 1px solid #D2D2D2;"  class="period"><?= $arElement['PROPERTIES']['PERIOD']['VALUE'] ?></div>
				<div style="border-right: 1px solid #D2D2D2;"  class="zakaz_f">
					<? if (number_format($arElement['PROPERTIES']['NEED']['VALUE'] + $arElement['PROPERTIES']['RESERVE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] - $arElement['PROPERTIES']['ITEM_IN_D']['VALUE'] - $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE']) > 0):?>
				<?= $arElement['PROPERTIES']['NEED']['VALUE'] + $arElement['PROPERTIES']['RESERVE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_BALANCE']['VALUE'] - $arElement['PROPERTIES']['ITEM_IN_D']['VALUE'] - $arElement['PROPERTIES']['AMOUNT_IN_STORAGE']['VALUE'] - $arElement['PROPERTIES']['CLIENT_STORAGE_IN']['VALUE'] ?>
				<?endif?>
				</div>

				<div style="border-right: 1px solid #D2D2D2;"  class="zakaz_s">
					<span><?= number_format($arElement['PROPERTIES']['ZAKAZ_S']['VALUE'], 3, ".","") ?></span>
					<input class="numer-num" type='text' name="zakazs[<?= $arElement["ID"]?>]" value='<?= $arElement['PROPERTIES']['ZAKAZ_S']['VALUE'] ?>' disabled>
				</div>

				<div style="border-right: 1px solid #D2D2D2;"  class="logistic"><?= $arElement['PROPERTIES']['LOGISTIC']['VALUE'] ?></div>
				
				<div style="border-right: 1px solid #D2D2D2;"  class="confirm_date"><?= $arElement['TIMESTAMP_X'] ?></div>
				
				<div style="border-right: 1px solid #D2D2D2;"   class="person"><?= $arElement['PROPERTIES']['PERSON']['VALUE'] ?></div>
				
				<div class="agree <?= $arElement['PROPERTIES']['FAGREE']['VALUE'] ? "checked" : "" ?>">					
					
					<? if(  $arElement['PROPERTIES']['FAGREE']['VALUE'] == 1 ) : ?>					
					<? else: ?>
					<input  name="fagree[<?= $arElement["ID"]?>]" type="checkbox" value="1">
					<? endif ?>
								
				</div>


								
	
				<div class="edit-wrap"><div class="edit"></div></div>
			</div>
			<? endif ?>
			<? endforeach ?>
		<? else: ?>
		<p style="text-align: center;"><font style="">Нет данных</font></p>
		<? endif ?>	
		</div>
	</div>	
	
	<? else : ?>
	<p style="text-align: center;"><font style="">Нет доступа</font></p>
	<? endif ?>
</form>

<?/*<div style="display:none">
	<div class="popup-numen" id="popup-numen">
		<span>ДОБАВИТЬ НОМЕНКЛАТУРУ</span>
		<form method="post" action="/ajax/orders_f.php" class="ajax-form-add-reference-information">
			<input type="hidden" id="element_id_firm" value="<?= $arParams["FILTER"]["PROPERTY_FIRM_ID"] ?>" name="firm_id">
			<input type="hidden" value="add" name="action">
			<div class="numen-table">				
				<div class="numen-items ">
					<div class="numen-items-head">
						<div class="numen-head-row">
							<div class="numen-check-block"> </div>
							<div class="">Период</div>
							<div class="article">Артикул</div>
							<div class="nomenclature">Номенклатура</div>
							<div class="measure">Ед.изм-я</div>
						</div>
					</div>
					<div class="numen-items-list ajax-catalog-items">
						
					</div>	
				</div>
			</div>
			<button class="numen-add-num-button" type="submit">Добавить номенклатурную позицию из каталога</button>
			<button class="numen-cancel-button" onclick=' parent.$.fancybox.close();' type="reset" >Отменить</button>
		</form>
	</div>
</div> */?>
