<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//echo "<pre>"; print_r($arParams); echo "</pre>";
//echo "<pre>"; print_r($arResult); echo "</pre>";
?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>

<div class="items">
	<div class="items-head">
		<div class="head-row">
			<div class="org-num">№ п/п</div>
			<div class="org">Организация</div>
			<div class="del-item"></div>
		</div>
	</div>	
	<div class="items-list">
	<? 	$i = $arResult["NAV_PAGE_NUM"]; ?>
	<? if (count($arResult["ITEMS"]) > 0): ?>
		<? foreach ($arResult["ITEMS"] as $arElement): ?>		
		<div class="item">
			<div class="org-num"><?= $i++ ?></div>
			<div class="org"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></div>
			<? if ($arResult["CAN_DELETE"] == "Y"):?>
				<?if ($arElement["CAN_DELETE"] == "Y"):?>
			<div class="del-item">
				<a class="delete-org" href="<?=$arElement["DELETE_LINK"]?>" onclick=""></a>
			</div>
				<? endif ?>
			<? endif ?>
		</div>		
		<? endforeach ?>
	<? endif ?>
	</div>	
</div>	
<? if (strlen($arResult["NAV_STRING"]) > 0):?><?= $arResult["NAV_STRING"] ?><?endif?>
<script>
	BX.ready(function() {
    
		$('.del-item a').confirm({
			title:"Удаление",
			text:"Действительно желаете удалить запись?",       
			confirmButton: "Да",
			cancelButton: "Нет",
			post: false
		});

	})
</script>