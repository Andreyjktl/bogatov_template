<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;

if (CModule::IncludeModule("iblock"))
{	

	if(!is_array($arParams["STATUS"]))
	{
		if($arParams["STATUS"] === "INACTIVE")
			$arParams["STATUS"] = array("INACTIVE");
		else
			$arParams["STATUS"] = array("ANY");
	}
	
	$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
	$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] != "N";
	$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
	$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
	$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
	$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]!=="N";

	$arGroups = $USER->GetUserGroupArray();

	// check whether current user has access to view list
	if ($USER->IsAdmin() || is_array($arGroups) && is_array($arParams["GROUPS"]) && count(array_intersect($arGroups, $arParams["GROUPS"])) > 0)
	{
		$bAllowAccess = true;
	}
	elseif ($USER->GetID() > 0 && $arParams["ELEMENT_ASSOC"] != "N")
	{
		$bAllowAccess = true;
	}
	else
	{
		$bAllowAccess = false;
	}

	// if user has access
	//if ($bAllowAccess)
	//{
		$arResult["CAN_EDIT"] = $arParams["ALLOW_EDIT"] == "Y" ? "Y" : "N";
		$arResult["CAN_DELETE"] = $arParams["ALLOW_DELETE"] == "Y" ? "Y" : "N";

		if ($USER->GetID())
		{
			$arResult["NO_USER"] = "N";			

			// set starting filter value
			$arFilter = array("IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"], "IBLOCK_ID" => $arParams["IBLOCK_ID"], "SHOW_NEW" => "Y");
			// check type of user association to iblock elements and add user association to filter
			//echo "<pre>"; print_r($arParams); echo "</pre>";

			/*if ($arParams["ELEMENT_ASSOC"] == "PROPERTY_ID" && intval($arParams["ELEMENT_ASSOC_PROPERTY"]) > 0 && in_array($arParams["ELEMENT_ASSOC_PROPERTY"], $arPropertyIDs))
			{
				$arFilter["PROPERTY_".$arParams["ELEMENT_ASSOC_PROPERTY"]] = $USER->GetID();
			}
			else
			{
				$arFilter["CREATED_BY"] = $USER->GetID();
			}*/

			// deleteting element			
			if (check_bitrix_sessid() && $_REQUEST["delete"] == "Y" && $arResult["CAN_DELETE"])
			{
				$arParams["ID"] = intval($_REQUEST["CODE"]);

				// try to get element with id, for user and for iblock
				$rsElement = CIBLockElement::GetList(array(), array_merge($arFilter, array("ID" => $arParams["ID"])));
				if ($arElement = $rsElement->GetNext())
				{
					// delete one
					$DB->StartTransaction();
					if(!CIBlockElement::Delete($arElement["ID"]))
					{
						$DB->Rollback();
					}
					else
					{
						$DB->Commit();
					}
				}
				//LocalRedirect($arParams['IBLOCK_URL'] . ($_SERVER['QUERY_STRING'] ? "?".$_SERVER['QUERY_STRING'] : ""));				
			}


			$arResult["ACTIVE_STATUS"] = array("Y" => GetMessage("IBLOCK_FORM_STATUS_ACTIVE"), "N" => GetMessage("IBLOCK_FORM_STATUS_INACTIVE"));
			
			// get elements list using generated filter				
			if ( $arParams["FILTER"] )
			{
				$arFilter = array_merge($arFilter, $arParams["FILTER"]);
			}
			
			//echo "<pre>";  var_dump($arFilter); echo "</pre>";
			
			$arNavParams = array(
				"nPageSize" => $arParams["NAV_ON_PAGE"],
				"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
				"bShowAll" => $arParams["PAGER_SHOW_ALWAYS"],			
			);			
			
			$bGetProperty = count($arParams["PROPERTY_CODE"])>0;		
			
			$rsIBlockElements = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, $arNavParams);
			$rsIBlockElements->SetUrlTemplates($arParams["DETAIL_URL"],"",$arParams["IBLOCK_URL"]);

			$arResult["ELEMENTS_COUNT"] = $rsIBlockElements->SelectedRowsCount();			

			// get current page elements to component result
			$arResult["ELEMENTS"] = array();
			$bCanEdit = false;
			$bCanDelete = false;
			while ($obElement = $rsIBlockElements->GetNextElement())
			{
			
				$arItem = $obElement->GetFields();

				$arButtons = CIBlock::GetPanelButtons(
					$arItem["IBLOCK_ID"],
					$arItem["ID"],
					0,
					array("SECTION_BUTTONS"=>false, "SESSID"=>false)
				);
				$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
				//$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
				$arItem["DELETE_LINK"] = "?delete=Y&amp;CODE=$arItem[ID]&amp;" . bitrix_sessid_get();
				

				if($arParams["PREVIEW_TRUNCATE_LEN"] > 0)
					$arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["PREVIEW_TEXT"], $arParams["PREVIEW_TRUNCATE_LEN"]);

				if(strlen($arItem["ACTIVE_FROM"])>0)
					$arItem["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));
				else
					$arItem["DISPLAY_ACTIVE_FROM"] = "";

				$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
				$arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();

				if(isset($arItem["PREVIEW_PICTURE"]))
				{
					$arItem["PREVIEW_PICTURE"] = (0 < $arItem["PREVIEW_PICTURE"] ? CFile::GetFileArray($arItem["PREVIEW_PICTURE"]) : false);
					if ($arItem["PREVIEW_PICTURE"])
					{
						$arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"];
						if ($arItem["PREVIEW_PICTURE"]["ALT"] == "")
							$arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["NAME"];
						$arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"];
						if ($arItem["PREVIEW_PICTURE"]["TITLE"] == "")
							$arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["NAME"];
					}
				}
				if(isset($arItem["DETAIL_PICTURE"]))
				{
					$arItem["DETAIL_PICTURE"] = (0 < $arItem["DETAIL_PICTURE"] ? CFile::GetFileArray($arItem["DETAIL_PICTURE"]) : false);
					if ($arItem["DETAIL_PICTURE"])
					{
						$arItem["DETAIL_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"];
						if ($arItem["DETAIL_PICTURE"]["ALT"] == "")
							$arItem["DETAIL_PICTURE"]["ALT"] = $arItem["NAME"];
						$arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"];
						if ($arItem["DETAIL_PICTURE"]["TITLE"] == "")
							$arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["NAME"];
					}
				}

				$arItem["FIELDS"] = array();
				foreach($arParams["FIELD_CODE"] as $code)
					if(array_key_exists($code, $arItem))
						$arItem["FIELDS"][$code] = $arItem[$code];

				if($bGetProperty)
					$arItem["PROPERTIES"] = $obElement->GetProperties();
				
				$arItem["DISPLAY_PROPERTIES"]=array();
				foreach($arParams["PROPERTY_CODE"] as $pid)
				{
					$prop = &$arItem["PROPERTIES"][$pid];
					if(
						(is_array($prop["VALUE"]) && count($prop["VALUE"])>0)
						|| (!is_array($prop["VALUE"]) && strlen($prop["VALUE"])>0)
					)
					{
						$arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "news_out");
					}
				}			
				
				$arItem["CAN_EDIT"] = $arResult["CAN_EDIT"];
				$arItem["CAN_DELETE"] = $arResult["CAN_DELETE"];


				if (!$bCanEdit && $arResult["CAN_EDIT"] == "Y" && $arItem["CAN_EDIT"] == "Y")
				{
					$bCanEdit = true;
				}

				if (!$bCanDelete && $arResult["CAN_DELETE"] == "Y" && $arItem["CAN_DELETE"] == "Y")
				{
					$bCanDelete = true;
				}
				
				$arResult["ITEMS"][] = $arItem;
				$arResult["ELEMENTS"][] = $arItem["ID"];				
			}	
			
			$arResult["NAV_STRING"] = $rsIBlockElements->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
			$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
			$arResult["NAV_RESULT"] = $rsIBlockElements;
			$this->SetResultCacheKeys(array(
				"ID",
				"IBLOCK_TYPE_ID",
				"LIST_PAGE_URL",
				"NAV_CACHED_DATA",
				"NAME",
				"SECTION",
				"ELEMENTS",
				"IPROPERTY_VALUES",
			));
			
			$arResult["NAV_PAGE_NUM"] = $_REQUEST["PAGEN_1"] > 1 ? ($_REQUEST["PAGEN_1"] * $arParams['NAV_ON_PAGE']) - $arParams['NAV_ON_PAGE'] + 1 : 1;
			
			if ($arResult["CAN_EDIT"] == "Y" && !$bCanEdit) $arResult["CAN_EDIT"] = "N";
			if ($arResult["CAN_DELETE"] == "Y" && !$bCanDelete) $arResult["CAN_DELETE"] = "N";		
			
		}
		else
		{
			$arResult["NO_USER"] = "Y";
		}
		
		//echo "<pre>"; var_dump($arResult["NAV_STRING"]); echo "</pre>";

		$arResult["MESSAGE"] = htmlspecialcharsex($_REQUEST["strIMessage"]);

		$this->IncludeComponentTemplate();
	/*}
	else
	{
		$APPLICATION->AuthForm("");
	}*/
}
?>
