<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");

if ( $USER->IsAdmin() )
	LocalRedirect("/lk/");
	
if( !$USER->IsAuthorized())
	LocalRedirect("/");
	
?><?$APPLICATION->IncludeComponent(
	"bitrix:currency.rates",
	"bg_template",
	Array(
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "86400",
		"arrCURRENCY_FROM" => array(0=>"USD",1=>"EUR",),
		"CURRENCY_BASE" => "RUB",
		"RATE_DAY" => "",
		"SHOW_CB" => "N"
	)
);?><br>
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket",
	".default",
	Array(
		"COLUMNS_LIST" => array(0=>"NAME",1=>"PROPS",2=>"DELETE",3=>"PRICE",4=>"QUANTITY",5=>"SUM",6=>"PROPERTY_ARTICLE",7=>"PROPERTY_VALUTA",8=>"PROPERTY_PRICEVAL",),
		"PATH_TO_ORDER" => "/personal/order.php",
		"HIDE_COUPON" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"USE_PREPAYMENT" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"ACTION_VARIABLE" => "action"
	)
);?> <br>
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.order.ajax",
	".default",
	Array(
		"PAY_FROM_ACCOUNT" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"COUNT_DELIVERY_TAX" => "N",
		"ALLOW_AUTO_REGISTER" => "N",
		"SEND_NEW_USER_NOTIFY" => "N",
		"DELIVERY_NO_AJAX" => "N",
		"DELIVERY_NO_SESSION" => "N",
		"DELIVERY_TO_PAYSYSTEM" => "d2p",
		"USE_PREPAYMENT" => "N",
		"ALLOW_NEW_PROFILE" => "Y",
		"SHOW_PAYMENT_SERVICES_NAMES" => "N",
		"SHOW_STORES_IMAGES" => "N",
		"PATH_TO_BASKET" => "/profile/",
		"PATH_TO_PERSONAL" => "/profile/statistics/",
		"PATH_TO_PAYMENT" => "",
		"PATH_TO_AUTH" => "/",
		"SET_TITLE" => "N",
		"DISABLE_BASKET_REDIRECT" => "Y",
		"PRODUCT_COLUMNS" => array(0=>"PROPERTY_ARTICLE",1=>"PROPERTY_VALUTA",2=>"PROPERTY_NAPRAVLENIE",3=>"PROPERTY_PRICEVAL",),
		"TEMPLATE_LOCATION" => ".default",
		"PROP_1" => array()
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>