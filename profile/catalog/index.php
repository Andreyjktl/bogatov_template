<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Формирование заявки");

if ( $USER->IsAdmin() )
	LocalRedirect("/lk/");

if( !$USER->IsAuthorized())
	LocalRedirect("/");	

$rsUser = CUser::GetByID( CUser::GetID() );
$arUser = $rsUser->Fetch();
$FIRM_ID = $arUser['UF_FIRM'];
$ACCESS_DIRECTORY = $arUser['UF_ACCESS_DIRECTORY']; 

$arrFilter = Array(/*"!CATALOG_QUANTITY"=>"0",*/ "PROPERTY_FIRM_ID" => $FIRM_ID);?> 

<?$APPLICATION->AddHeadString('<meta http-equiv="Cache-Control" content="max-age=3600, must-revalidate">',true)?> 
<?$APPLICATION->AddHeadString('<meta http-equiv="Cache-Control" content="max-age=3600, proxy-revalidate">',true)?> 
 <br>
<H3><?= $arUser['WORK_COMPANY']?></H3>
<br>


 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.smart.filter",
	"visual_horizontal_b",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "3",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SAVE_IN_SESSION" => "Y",
		"INSTANT_RELOAD" => "N",
		"PRICE_CODE" => array(),
		"XML_EXPORT" => "N",
		"SECTION_TITLE" => "-",
		"SECTION_DESCRIPTION" => "-",
		"TEMPLATE_THEME" => "blue"
	)
);?><br>
 <?$APPLICATION->IncludeComponent(
	"bitrix:currency.rates",
	"bg_template_2",
	Array(
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "86400",
		"arrCURRENCY_FROM" => array(0=>"USD",1=>"EUR",),
		"CURRENCY_BASE" => "RUB",
		"RATE_DAY" => "",
		"SHOW_CB" => "Y"
	)
);?> 

<?php
// тут выставляем переменную шаблона
$myTemlates = array(
     'listn',
     'listn_priceless'
   ); // имена шаблонов которые у вас реально есть
if ($FIRM_ID == 369) {
   $templateName = 'listn';
} else {
   $templateName = 'listn_priceless';
}
// на выходе у нас есть уже существующее имя шаблона 
?>

 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	$templateName,
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "3",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"ELEMENT_SORT_FIELD" => "name",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "",
		"ELEMENT_SORT_ORDER2" => "",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "60",
		"LINE_ELEMENT_COUNT" => "5",
		"PROPERTY_CODE" => array(0=>"ARTICLE",1=>"VALUTA",2=>"RESERVED",3=>"IN_DELIVERY",4=>"PRICEVAL",5=>"",),
		"OFFERS_LIMIT" => "5",
		"TEMPLATE_THEME" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => array(0=>"BASE_PRICE",),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "Y",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(),
		"PAGER_TEMPLATE" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"SET_BROWSER_TITLE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity"
	)
);?> <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>