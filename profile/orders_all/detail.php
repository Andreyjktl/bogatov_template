<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Детальный обзор элемента");

if ( $USER->IsAdmin() )
	LocalRedirect("/lk/");

if( !$USER->IsAuthorized())
	LocalRedirect("/");	
	
$rsUser = CUser::GetByID( CUser::GetID() );
$arUser = $rsUser->Fetch();
$FIRM_ID = $arUser['UF_FIRM'];
$ACCESS_DIRECTORY = $arUser['UF_ACCESS_DIRECTORY'];
$arProd = 0;
?><br>
<table class="" cellpadding="0" cellspacing="10" height="30" width="100%">
<tbody>
<tr>
	<td>
		 &nbsp; <a href="/profile/orders_all/index.php" style="color: #FF6700; border-bottom: 2px solid #FF6700; text-transform: uppercase;">Сводная таблица</a><br>
	</td>
	<td>
		 &nbsp;<a href="/profile/order_f/index.php" style="color: #585F69; text-transform: uppercase;">Первичный расчет</a><br>
	</td>
	<td>
		 &nbsp; <a href="/profile/order_s/index.php" style="color: #585F69; text-transform: uppercase;">Вторичный расчет</a><br>
	</td>
	<td>
		 &nbsp; <a href="/profile/commit/index.php" style="color: #585F69; text-transform: uppercase;">Утверждение заказа</a><br>
	</td>
</tr>
</tbody>
</table>
 <?global $ID;?> <?$APPLICATION->IncludeComponent(
	"pl:iblock.element.list",
	"orders_detail",
	Array(
		"FILTER" => array("PROPERTY_FIRM_ID"=>$FIRM_ID),
		"ACCESS_DIRECTORY" => ($ACCESS_DIRECTORY==1?"Y":"N"),
		"DETAIL_URL" => "",
		"IBLOCK_URL" => "/lk/orders_all/",
		"PAGER_SHOW_ALWAYS" => "N",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "50",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "8",
		"GROUPS" => array(),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "N",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "",
		"PROPERTY_CODE" => array("CML2_LINK","SV_LOGISTIC","SV_PERIOD","SV_UPAK"),
		"VARIABLE_ALIASES" => Array(),
		"VARIABLE_ALIASES" => Array(
		)
	)
);?> 
 <?$APPLICATION->IncludeComponent(
	"pl:iblock.element.list",
	"orders_detail_table",
	Array(
		"FILTER" => array("PROPERTY_FIRM_ID"=>$FIRM_ID),
		"ACCESS_DIRECTORY" => ($ACCESS_DIRECTORY==1?"Y":"N"),
		"DETAIL_URL" => "",
		"IBLOCK_URL" => "/lk/orders_all/",
		"PAGER_SHOW_ALWAYS" => "N",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "50",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "6",
		"GROUPS" => array(),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "N",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "",
		"PROPERTY_CODE" => array("CML2_LINK", "FIRM_ID", "CLIENT_BALANCE", "CLIENT_STORAGE_IN","NEED", "RESERVE", "DATE_IN", "ITEM_IN", "AMOUNT_IN_STORAGE", "FAGREE", "ARTICLE", "UNITS", "PERIOD", "ZAKAZ_F"),
		"VARIABLE_ALIASES" => Array(),
		"VARIABLE_ALIASES" => Array(
		)
	)
);?> 

<?/* Скрипт с выводом высоты страницы*/?>
<script>
 <?/* window.onload = function() {
            //alert(window.innerHeight);
            alert(document.body.scrollHeight);
            if (document.body.scrollHeight > 1500){
            location.reload();}
        }*/?>

        var timerId = setInterval(function() {
  			if (document.body.scrollHeight > 4000){
           			 location.reload();}
}, 1000);
</script>


 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>