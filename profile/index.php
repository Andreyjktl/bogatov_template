<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Формирование заявки");
$arrFilter = Array("!CATALOG_QUANTITY"=>"0");

if ( $USER->IsAdmin() )
	LocalRedirect("/lk/");
elseif($USER->IsAuthorized())
	LocalRedirect("/profile/catalog/");
else
	LocalRedirect("/");


?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>