<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Состав заказа");
?><?$APPLICATION->IncludeComponent(
	"bitrix:sale.personal.order.detail",
	"",
	Array(
		
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"PREVIEW_PICTURE_WIDTH" => "110",
		"PREVIEW_PICTURE_HEIGHT" => "110",
		"DETAIL_PICTURE_WIDTH" => "110",
		"DETAIL_PICTURE_HEIGHT" => "110",
		"RESAMPLE_TYPE" => "0",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "Y",
		"PATH_TO_LIST" => "/profile/statistics/",
		"PATH_TO_CANCEL" => "",
		"PATH_TO_PAYMENT" => "payment.php",
		"ID" => $ID,
		"SET_TITLE" => "N",
		"CUSTOM_SELECT_PROPS" => array("PROPERTY_ARTICLE")		
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>