<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Согласование заказа поставщиком");

if ( $USER->IsAdmin() )
	LocalRedirect("/lk/");

if( !$USER->IsAuthorized())
	LocalRedirect("/");	
	
$rsUser = CUser::GetByID( CUser::GetID() );
$arUser = $rsUser->Fetch();
$FIRM_ID = $arUser['UF_FIRM'];
$ACCESS_DIRECTORY = $arUser['UF_ACCESS_DIRECTORY']; 

?>
<?$APPLICATION->IncludeComponent(
	"pl:iblock.element.list",
	"commit",
	Array(
		"FILTER" => array("PROPERTY_FIRM_ID"=>$FIRM_ID),
		"ACCESS_DIRECTORY" => ($ACCESS_DIRECTORY==1?"Y":"N"),
		"DETAIL_URL" => "",
		"IBLOCK_URL" => "/lk/commit/",
		"PAGER_SHOW_ALWAYS" => "N",
		"EDIT_URL" => "",
		"NAV_ON_PAGE" => "20",
		"MAX_USER_ENTRIES" => "100000",
		"IBLOCK_TYPE" => "firm",
		"IBLOCK_ID" => "4",
		"GROUPS" => array(),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "N",
		"ALLOW_EDIT" => "Y",
		"ALLOW_DELETE" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "",
		"PROPERTY_CODE" => array("ARTICLE","UNITS","NEED","RESERVE","FAGREE"),
		"VARIABLE_ALIASES" => Array(),
		"VARIABLE_ALIASES" => Array(
		)
	)
);?><br>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>